#!/usr/bin/env python
#-*- coding:utf-8 -*-

# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


try:
    import simplejson as json
except ImportError:
    import json


import re
import urlparse
import urllib
# from aawiki.models import name_urlescape
from django.core.urlresolvers import reverse

wikify_protectedchars = re.compile(r"[?+]", re.I)
# re.compile(r"[^a-zA-Z0-9\-()_]", re.I)


def name_normalize(n):
    """ Normalizes the name: strips whitespace & capitalizes the first letter
    """

    n = n.strip()
    if (len(n) > 0):
        n = n[0].upper() + n[1:]
    return n


def name_urlescape(n):
    """ normal to url: Normalizes, then replaces spaces with underscore, and
    escapes other 'protected' chars """

    n = name_normalize(n)
    n = n.replace(" ", "_")

    def escape(n):
        char = n.group(0)
        return urllib.quote(char)
    n = wikify_protectedchars.sub(escape, n)
    return n


def name_urlunescape(n):
    """ url to normal """
    n = n.replace("_", " ")
    if (type(n) == unicode):
        n = n.encode("utf-8")
    n = urllib.unquote(n)
    return n


class LinkForm:
    """
    full form:

    [[ rel ::? item # fragment | label ]]

    only item is required, aka:
        [rel ::?]item[#fragment][| label]

    rel
    item (normalized name)
    fragment
    label

    """

    # this pattern failed on two consecutive links that could be read as one
    # pat = re.compile(r"\[\[\s*(?:(.+?)\s*::)?\s*(?:(.+?)\s*:)?\s*(.+?)\s*(?:\|\s*(.+?)\s*)?\s*\]\]")
    # thus . => [^\]] (anything but a close bracket), in the spirit of minimally limiting.

#    pat = re.compile(r"\[\[\s*(?:(?P<rel>[^\]]+?)\s*::)?\s*(?:(?P<namespace>[^\]]+?)\s*:)?\s*(?P<item>[^\]]+?)\s*(?:\|\s*(?P<label>[^\]]+?)\s*)?\s*\]\]")
#    pat = re.compile(r"\[\[\s*(?:(?P<rel>[^\]#]+?)\s*::)?\s*(?:(?P<namespace>[^\]#]+?)\s*:)?\s*(?P<item>[^\]#]+?)\s*(?:#\s*(?P<fragment>[^\]]+?))?\s*(?:\|\s*(?P<label>[^\]]+?)\s*)?\s*\]\]")

    # rel, item, frag, label
    # pat = re.compile(r"\[\[\s*(?P<contents>(?:(?P<rel>[^\]#]+?)\s*::?)?\s*(?P<item>[^\]#]+?)\s*(?:#\s*(?P<fragment>[^\]]+?))?\s*(?:\|\s*(?P<label>[^\]]+?)\s*)?)\s*\]\]")
    pat = re.compile(r"""\[\[ \s*
        (?P<contents>
            (?:(?P<rel>[^\]#]+?) \s* ::?)? \s*
            (?P<item>[^\]#]+?)\s*
            (?:\# \s* (?P<fragment>[^\]]+?) )? \s*
            (?:\| \s* (?P<label>[^\]]+?) \s*)?
        ) \s*
    \]\]""", re.X)

    # White space-preserving pattern
    ppat = re.compile(r"""\[\[
        (?P<rel> \s* (?P<rel_name> [^\]#]+? ) \s* ::?)?
        (?P<item> \s* (?P<item_name> [^\]#]+? ) \s* )
        (?P<fragment> \# (?P<fragment_name> [^\]]+? ) \s* )?
        (?P<label> \| \s* (?P<label_name> [^\]]+? ) \s*)?
    \]\]""", re.X)

    # (?:#\s*(?P<fragment>[^\]]+?))?
    def __init__(self, match):
        self.source = match.group(0)
        d = match.groupdict()
        # HACK! CHECK IF ORIGINAL looks like a URL (http only at the moment!)
        contents = d['contents']
        urlparts = urlparse.urlparse(contents)
        if urlparts:
            scheme = urlparts[0]
            if scheme.lower() == "http":
                self.rel = None
                self.item = contents
                self.fragment = None
                self.label = None
                try:
                    hashpos = contents.rindex("#")
                    self.item = contents[:hashpos]
                    self.fragment = contents[hashpos + 1:]
                except ValueError:
                    pass
                return

        self.rel = d['rel']
        self.item = d['item']
        self.fragment = d['fragment']
        self.label = d['label']
#        for (key, val) in match.groupdict().iteritems():
#            setattr(self, key, val)

    def unparse(self):
        ret = u"[[ "
        if (self.rel):
            ret += self.rel
            ret += u":: "
        ret += self.item + u" "
        if self.fragment:
            ret += "# " + self.fragment + " "
        if self.label:
            ret += self.label + " "
        ret += "]]"
        return ret

    def debug(self):
        return "[[ %s :: %s # %s | %s ]]" % ((self.rel or ""), self.item, \
                (self.fragment or ""), (self.label or ""))

# linkpat = re.compile(r"\[\[[\w()|/ ]+\]\]")


def Link_parseText(text):
    """ translate text to a list of Link objects """
    ret = []
    for match in LinkForm.pat.finditer(text):
        ret.append(LinkForm(match))
    return ret


def Link_sub(sub, text):
    return LinkForm.pat.sub(sub, text)


def Link_markup_sub(match):
    link = LinkForm(match)
    label = link.label or link.item
    rel = link.rel or "link"
    item = link.item
    if not item.startswith("http://"):
        item = reverse("aabrowser.views.playlist", args=[name_urlescape(item)])

    return """<a href="%s" rel="%s" class="tag">%s</a>""" \
        % (item, rel, label)


def Link_markup(text):
    """ translate text to a list of Link objects """
    return LinkForm.pat.sub(Link_markup_sub, text)

if __name__ == "__main__":

    text = u"""

[[Inès Rabadan|test]]
[[recorded:8 April 2008 ]]

"""

    def unparseFromDict(d):
        return u"[[" + (d['rel'] or "") + (d['item'] or "") + \
                (d['fragment'] or "") + (d['label'] or "") + u"]]"

    def unparseNorm(m):
        d = m.groupdict()
        parts = []
        if d['rel_name']:
            parts.append("rel: %s" % d['rel_name'])
        parts.append("item: %s" % d['item_name'])
        if d['fragment_name']:
            parts.append("fragment: %s" % d['fragment_name'])
        if d['label_name']:
            parts.append("label: %s" % d['label_name'])
        return u"<Link " + ", ".join(parts) + u">"

    for m in LinkForm.ppat.finditer(text):
        print unparseNorm(m)
        d = m.groupdict()
        print unparseFromDict(d)

    def findAndReplaceLinkNames(oldname, newname, text):

        def sub(match):
            return "*"

        return LinkForm.pat.sub(sub, text)

    print findAndReplaceLinkNames("", "", text)
