from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('aabrowser.views',
    (r'^srt/$', 'srt'),

    # The home page redirects to the most recent changes
    url('^$', 'home', {}, name='aa-resources'),

    url(r'^search/$', 'search', {}, name='aa-search'),
    url(r'^add/$', 'add', {}, name='aa-add'),
    url(r'^resources/(?P<id>\d+)/$', 'resource', {}, name='aa-resource'),
    url(r'^resources/(?P<id>\d+)/annotate/$', 'resource_annotate'),
    url(r'^resources/(?P<id>\d+)/position/$', 'resource_position'),
    url(r'^resources/(?P<id>\d+)/size/$', 'resource_size'),
    url(r'^resources/(?P<id>\d+)/delete/$', 'resource_delete'),
    url(r'^resources/info/$', 'resource_info'),

    # Order resources by...
    url(r'^recent/$', 'resources', {'ordering': '-db_lastmodified'}, \
            name='aa-recent'),

    # Filter resources by type
    url(r'^video/$', 'resources', {'rtype': 'video'}, name='aa-video'),
    url(r'^audio/$', 'resources', {'rtype': 'audio'}, name='aa-audio'),
    url(r'^image/$', 'resources', {'rtype': 'image'}, name='aa-image'),
    url(r'^html/$', 'resources', {'rtype': 'html'}, name='aa-html'),
    url(r'^text/$', 'resources', {'rtype': 'text'}, name='aa-text'),

    url(r'^annotations/(?P<id>\d+)/$', 'annotation', {}, name='aa-annotation'),
    url(r'^annotations/(?P<id>\d+)/position/$', 'annotation_position', {}, \
            name='aa-annotation-position'),
    url(r'^annotations/(?P<id>\d+)/size/$', 'annotation_size', {}, \
            name='aa-annotation-size'),
    url(r'^annotations/(?P<id>\d+)/delete/$', 'annotation_delete', {}, \
            name='aa-annotation-delete'),
    url(r'^annotations/(?P<id>\d+)/export/srt/$', 'annotation_export', {}, \
            name='aa-annotation-export-srt'),
    url(r'^annotations/(?P<id>\d+)/export/audacity/$', 'annotation_export', \
            {'template': 'annotation.audacity.txt'}, \
            name='aa-annotation-export-audacity'),
    url(r'^annotations/(?P<id>\d+)/import/audacity/$', \
            'annotation_import_audacity',  {}, \
            name='aa-annotation-import-audacity'),

    url(r'^linktool/$', 'linktool', {}, name='aa-linktool'),
    url(r'^linktool/search/$', 'linktool_search', {}, \
            name='aa-linktool-search'),

    url(r'^tag/edit/(?P<name>.+)/$', 'tag', {}, name='aa-tag-edit'),
    url(r'^tag/rename/(?P<name>.+)/$', 'tag_rename', {}, name='aa-tag-rename'),
    url(r'^replace/$', 'searchandreplace', {}, name='aa-searchandreplace'),
    url(r'^tagstool/$', 'tagstool', {}, name='aa-tagstool'),
    url(r'^tags/$', 'tags', {}, name='aa-tags'),
    url(r'^tags/(?P<names>.+)/$', 'playlist', name='aa-playlist'),
)
