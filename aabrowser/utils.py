from .timecode import timecode_fromsecs


def audacity2srt(data):
    """docstring for audacity2srt"""
    srt = ""
    for line in data.splitlines():
        chunks = line.split(' ')
        start = timecode_fromsecs(float(chunks[0].replace(',', '.')), \
                alwaysfract=True, alwayshours=True, fractdelim='.')
        end = timecode_fromsecs(float(chunks[1].replace(',', '.')), \
                alwaysfract=True, alwayshours=True, fractdelim='.')
        chunks[0] = start
        chunks[1] = end
        srt += chunks[0] + "-->" + chunks[1] + "\n" + " ".join(chunks[2:]) \
                + "\n"
    srt = srt.replace('\\n', '\n')
    return srt
