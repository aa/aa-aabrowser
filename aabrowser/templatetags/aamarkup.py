# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


from django.template.defaultfilters import stringfilter
from django import template
# from django.utils.safestring import mark_safe
# from django.utils.html import conditional_escape
import urllib
import aabrowser.timecode
from aabrowser.LinkForm import Link_markup

# from aabrowser.LinkForm import Link_markup
# import aabrowser.sectionparser
# import aabrowser.humanize_bytes

register = template.Library()


def _humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """
    abbrevs = (
        (1 << 50L, 'PB'),
        (1 << 40L, 'TB'),
        (1 << 30L, 'GB'),
        (1 << 20L, 'MB'),
        (1 << 10L, 'kB'),
        (1, 'bytes')
    )
    if bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    return '%.*f %s' % (precision, bytes / factor, suffix)


@stringfilter
def humanize_bytes(value, autoescape=None):
    return _humanize_bytes(long(value))

register.filter('humanize_bytes', humanize_bytes)


def _humanize_seconds(value):
    value = float(value)
    (hours, minutes, seconds) = (0.0, 0.0, 0.0)
    parts = []
    if value > 3600:
        hours = int(value) / 3600
        value -= hours * 3600
        parts.append("%dhour" % hours)
    if value > 60:
        minutes = int(value) / 60
        value -= minutes * 60
        parts.append("%dmin" % minutes)

    seconds = value
    if seconds:
        parts.append("%dsec" % int(seconds))

    return ", ".join(parts)


@stringfilter
def humanize_seconds(value, autoescape=None):
    return _humanize_seconds(value)

register.filter('humanize_seconds', humanize_seconds)


@stringfilter
def urldecode(value, autoescape=None):
    return urllib.unquote(value)

register.filter('urldecode', urldecode)


@stringfilter
def secs2timecode(value, autoescape=None):
    value = float(value)
    return aabrowser.timecode.timecode_fromsecs(value)

register.filter('secs2timecode', secs2timecode)


@stringfilter
def secs2srttimecode(value, autoescape=None):
    value = float(value)
    return aabrowser.timecode.timecode_fromsecs(value, alwaysfract=True, \
            alwayshours=True)

register.filter('secs2srttimecode', secs2srttimecode)


@stringfilter
def tosingleline(value, autoescape=None):
    """Escapes newlines"""
    return value.replace('\n', '\\n')

register.filter('tosingleline', tosingleline)


@stringfilter
def tomultilines(value, autoescape=None):
    """Unescapes newlines"""
    return value.replace('\\n', '\n')
register.filter('tomultilines', tomultilines)


@stringfilter
def aalinks(value, autoescape=None):
    value = Link_markup(value)
    return value

register.filter('aalinks', aalinks)

#@stringfilter
#def aamarkup(value, autoescape=None):
#    if autoescape:
#        esc = conditional_escape
#    else:
#        esc = lambda x: x
#    # sections
#    doc = aabrowser.sectionparser.parse(value)
#    ret = u""
#    for s in doc.sections:
#        if s.depth >= 1 and s.name:
#            ret += u"<h%d>%s</h%d>\n" % (s.depth, s.name, s.depth)
#        stext = s.getText().strip()
#        stext = stext.replace("\n", "<br />")
#        stext = Link_markup(stext)
#        ret += stext
#    # value = Link_markup(value)
#    return mark_safe(ret)

#register.filter('aamarkup', aamarkup)

#@stringfilter
#def markup_links(value, autoescape=None):
#    return mark_safe(Link_markup(value))

#register.filter('aamarkuplinks', markup_links)
