1
00:00:01,838 --> 00:00:03,827
The Creative Union
of Comedy and Musical Films

2
00:00:20,119 --> 00:00:21,119
Presents

3
00:00:21,120 --> 00:00:28,440
Kin Dza Dza

4
00:00:42,080 --> 00:00:47,280
Screenplay
Revaz Gabriadze
Giorgi Danelia

5
00:00:47,720 --> 00:00:52,040
Director
Giorgi Danelia

6
00:02:05,523 --> 00:02:06,523
Cinematography
Paval Lebeshev

7
00:02:06,524 --> 00:02:07,524
Design
Aleksandr Samulekin
Theodor Tezik

8
00:02:07,525 --> 00:02:08,525
Composer
Gia Kancheli

9
00:02:08,526 --> 00:02:09,526
Sound
Yekaterina Popova

10
00:02:09,527 --> 00:02:10,527
Assistant Director
Sergei Skripka
