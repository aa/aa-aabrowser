# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


"""
SRT.py
classes to represent & parse SRT subRip subtitle files
"""

import cgi  # for cgi.escape of titles
import re
import sys
import timecode

## EXAMPLE SRT FILE:
#1
#00:00:48,850 --> 00:00:50,820
#There's an old joke.
#
#2
#00:00:50,900 --> 00:00:53,740
#Two elderly women are
#at a Catskill mountain resort.

# technically the separator should be a comma, but the pattern accepts dots too
# timecode_pat = re.compile(r"^(?:(\d\d):)?(\d\d):(\d\d)([,.]\d\d\d)?(\s*-->\s*(?:(\d\d):)?(\d\d):(\d\d)([,.]\d\d\d)?)?", re.M)
timecode_pat = re.compile(r"^(?:(\d\d):)?(\d\d):(\d\d)([,.]\d\d\d)?\s*-->\s*((?:(\d\d):)?(\d\d):(\d\d)([,.]\d\d\d)?)?", re.M)
number = re.compile(r"^\d+$")


def get_timecodes(match):
    """
    translates a match object into tuple of 1 or 2 4-tuples as in:
    (HH, MM, SS, FRAMES)
    sets FRAMES to 0 if none specified
    """
    g = match.groups()
    tc1 = None
    tc2 = None

    # tc1 is 0,1,2 + 3?
    # tc2 is 5,6,7 + 8?

    if g[3]:
        if g[0]:
            tc1 = (int(g[0]), int(g[1]), int(g[2]), int(g[3][1:]))
        else:
            tc1 = (0, int(g[1]), int(g[2]), int(g[3][1:]))
    else:
        if g[0]:
            tc1 = (int(g[0]), int(g[1]), int(g[2]), 0)
        else:
            tc1 = (0, int(g[1]), int(g[2]), 0)

    if g[4]:
        if g[8]:
            if g[5]:
                tc2 = (int(g[5]), int(g[6]), int(g[7]), int(g[8][1:]))
            else:
                tc2 = (0, int(g[6]), int(g[7]), int(g[8][1:]))
        else:
            if g[5]:
                tc2 = (int(g[5]), int(g[6]), int(g[7]), 0)
            else:
                tc2 = (0, int(g[6]), int(g[7]), 0)

    if tc2:
        return (tc1, tc2)
    else:
        # nb the importance of the extra comma to return a 1 element list!
        return (tc1,)


def timecodelist_to_secs(tc):
    # print "TCUNPARSE: " + str(tc)
    if len(tc) == 4:
        fract_str = str(tc[3])
        # append leading zeros if necessary
        while len(fract_str) < 3:
            fract_str = "0" + fract_str
        secs = (tc[0] * 3600) + (tc[1] * 60) + tc[2] + float("0." + fract_str)
    else:
        secs = (tc[0] * 3600) + (tc[1] * 60) + tc[2]
    return secs


def match_to_secs(m):
    return [timecodelist_to_secs(x) for x in get_timecodes(m)]


class SRTSequenceException(Exception):
    pass


class SRT:
    def __init__(self):
        self.titles = []

    def unparse_xml(self, inCDATA=False):
        ret = "<srt>\n"
        for t in self.titles:
            ret += t.unparse_xml(inCDATA)
        ret += "</srt>\n"
        return ret

    def unparse(self, number=True):
        ret = ""
        count = 1
        for t in self.titles:
            if number:
                ret += str(count) + "\n"
            count += 1
            ret += t.unparse()
        return ret

    def addTitle(self, text, start, end=None):
        """
        addTitle(text, start, [end])
        start & end may be string timecodes or numeric seconds
        """
        if (type(start) == str):
            start = timecode.timecode_tosecs(start)
        if (end != None and type(end) == str):
            end = timecode.timecode_tosecs(end)
        self.titles.append(Title(start, end, text))

    def normalize_endtimes(self, duration=None):
        """
        sets end times on titles
        also enforce sorted order, non-overlapping, and
        adding buffer titles as needed
        duration, if given, is used for the final title

        titles must have non-null start times
        """

        nostarts = []
        starts = []
        for title in self.titles:
            if title.start == None:
                nostarts.append(title)
            else:
                starts.append(title)
        starts.sort(key=lambda x: x.start)

        curtime = 0.0
        ti = 0
        while (ti < len(starts)):
            t = starts[ti]
            if (t.start > curtime):
                # starts.insert(ti, Title(curtime, t.start, None))
                curtime = t.start
                # continue

            if (t.end == None):
                if (ti + 1 < len(starts)):
                    t.end = starts[ti + 1].start
                elif duration != None:
                    t.end = duration

            curtime = t.end
            ti += 1

        self.titles = nostarts + starts

    def normalize(self, duration=None):
        """
        sets end times on titles
        duration, if given & needed, is used for the final title
        titles must have non-null start times
        """
        curtime = 0.0
        ti = 0
        while (ti < len(self.titles)):
            t = self.titles[ti]
            if (t.start < curtime):
                raise SRTSequenceException("title start earlier than preceding title")
            if (t.start > curtime):
                self.titles.insert(ti, Title(curtime, t.start, None))
                curtime = t.start
                continue
            # ensure end time is set on current title
            if (t.end == None):
                if (ti + 1 < len(self.titles)):
                    t.end = self.titles[ti + 1].start
                elif duration != None:
                    t.end = duration
            # adjust curtime to end
            curtime = t.end
            ti += 1
        # fill any remaining gap to the final duration (if given)
        if (duration != None and curtime != duration):
            self.titles.append(Title(curtime, duration, None))


class Title:
    def __init__(self, start=None, end=None, text=None):
        self.start = start
        self.end = end
        self.text = text

    def unparse_xml(self, inCDATA=False):
        if (self.end != None and self.start != None):
            ret = "<title start=\"%s\" end=\"%s\" startsecs=\"%0.3f\" endsecs=\"%0.3f\">\n" % (timecode.timecode_fromsecs(self.start), timecode.timecode_fromsecs(self.end), self.start, self.end)
        elif (self.start != None):
            ret = "<title start=\"%s\" startsecs=\"%0.3f\">\n" % (timecode.timecode_fromsecs(self.start), self.start)
        else:
            ret = "<title>\n"

        if (self.text == None):
            # MAKE SELF CLOSING TAG
            ret = ret.strip().rstrip("> ")
            ret += " />"
            return ret

        if inCDATA:
            ret += "<![CDATA[\n"
            ret += self.text + "\n"
            ret += "]]>\n"
        else:
            ret += cgi.escape(self.text) + "\n"
        ret += "</title>\n"
        return ret

    def unparse(self):
        ret = timecode.timecode_fromsecs(self.start, True, True)
        ret += " -->"
        if (self.end):
            ret += " " + timecode.timecode_fromsecs(self.end, True, True)
        ret += "\n"
        ret += self.text + "\n\n"
        return ret


class SRTParser:

    def __init__(self, blanklinesseparate=False):
        self.blanklinesseparate = blanklinesseparate

    ### "PUBLIC"

    def parse(self, file):
        self.parseBegin()
        for line in file:
            line = line.rstrip()
            self.parseLine(line)
        return self.parseEnd()

    def parseString(self, text):
        self.parseBegin()
        for line in text.splitlines():
            line = line.rstrip()
            self.parseLine(line)
        return self.parseEnd()

    def unparse_xml(self):
        titles_unparse_xml(self.titles)

    ### "PRIVATE"

    def parseBegin(self):
        self.srt = SRT()
        # self.titles = []
        self.tcodes = None
        self.curlines = []

    def parseEnd(self):
        if self.tcodes or self.curlines:
            self.newtitle()
        ret = self.srt
        del self.srt
        return ret

    def newtitle(self):
        # eliminate any preceding single line with a number
        #if len(self.curlines) == 1 and number.match(self.curlines[0]):
            #self.curlines = []
        # is the last line a number line (if so, suppress as it may be an SRT line number)
        if len(self.curlines) >= 1 and number.match(self.curlines[-1]):
            self.curlines = self.curlines[:-1]

        # if (self.tcodes and self.curlines):
        # if self.allowBlankTitles or self.curlines:
        if self.curlines or self.tcodes:
            text = "\n".join(self.curlines).strip()
            if (self.tcodes != None and len(self.tcodes) == 2):
                t = Title(timecodelist_to_secs(self.tcodes[0]), \
                        timecodelist_to_secs(self.tcodes[1]), text)
            elif (self.tcodes != None and len(self.tcodes) == 1):
                t = Title(timecodelist_to_secs(self.tcodes[0]), None, text)
            else:
                t = Title(None, None, text)

            self.srt.titles.append(t)

        self.tcodes = None
        self.curlines = []

    def parseLine(self, line):
        m = timecode_pat.match(line)
        if (m):
            self.newtitle()
            self.tcodes = get_timecodes(m)
        elif (self.blanklinesseparate and (line.strip() == "")):
            self.newtitle()
            self.tcodes = None
        else:
            self.curlines.append(line)


def parse(t, blanklinesseparate=False):
    p = SRTParser(blanklinesseparate)
    if (type(t) == str or type(t) == unicode):
        return p.parseString(t)
    else:
        return p.parse(file)

if (__name__ == "__main__"):
    p = SRTParser()
    srt = p.parse(sys.stdin)
    # srt.normalize_endtimes()
    print srt.unparse_xml()
#    for t in srt.titles:
#        print t.start

    # srt.normalize()
    # print srt.unparse(False)
    # print srt.unparse_xml()
