# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


from django.contrib import admin
from django.db import models
from Resource import Resource

"""
minimal generic resource linking
"""

RELATION_TYPES = (
    ("thumbnail", "thumbnail"),
    ("version", "version"),
)


class Link (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("src",)

    src = models.ForeignKey(Resource, related_name="links_as_src")
    rel = models.CharField(max_length=255, choices=RELATION_TYPES)
    dest = models.ForeignKey(Resource, related_name="links_as_dest")


class LinkAdmin(admin.ModelAdmin):
    list_display = ('src', 'rel', 'dest')
    list_display_links = ('src', )

try:
    admin.site.register(Link, LinkAdmin)
except admin.sites.AlreadyRegistered:
    pass
