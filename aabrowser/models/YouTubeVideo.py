# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


from aabrowser.templatetags.aamarkup import _humanize_seconds, _humanize_bytes
from Annotation import Annotation, Section
from django.db import models
from Link import Link
from Resource import Resource, ResourceSniffer, getResource
import aabrowser.iso8601
import gdata.youtube.service
import re

youtubevid_pat = re.compile(r"""^http://(www\.)?youtube\.com/watch""" \
        + """\?v=(?P<id>[^&]+)""", re.I)


class YouTubeVideo (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource", )

    resource = models.ForeignKey(
            Resource,
    )
    youtube_id = models.CharField(
            max_length=255,
            blank=True,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )
    title = models.CharField(
            max_length=255,
            blank=True,
    )
    published_raw = models.CharField(
            max_length=255,
            blank=True,
    )
    published = models.DateTimeField(
            null=True,
            blank=True,
    )
    description = models.CharField(
            max_length=255,
            blank=True,
    )
    category = models.CharField(
            max_length=255,
            blank=True,
    )
    tags = models.CharField(
            max_length=255,
            blank=True,
    )
    watch_url = models.CharField(
            max_length=255,
            blank=True,
    )
    flash_player_url = models.CharField(
            max_length=255,
            blank=True,
    )
    view_count = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    location = models.CharField(
            max_length=255,
            blank=True,
    )
    rating = models.CharField(
            max_length=255,
            blank=True,
    )

    def syncRequest(self, data):
        self.youtube_id = data.youtube_id
        self.duration = float(data.youtube.media.duration.seconds)
        self.title = data.youtube.media.title.text
        self.published_raw = data.youtube.published.text
        if self.published_raw:
            self.published = aabrowser.iso8601.parse_date(self.published_raw)
        else:
            self.published = None
        self.description = data.youtube.media.description.text or ""
        self.category = data.youtube.media.category[0].text
        self.tags = data.youtube.media.keywords.text
        self.watch_url = data.youtube.media.player.url
        self.flash_player_url = data.youtube.GetSwfUrl()
        if data.youtube.statistics:
            # stats can be null (on a new unwatched clip, sep 2010 mm)
            self.view_count = data.youtube.statistics.view_count

        if data.youtube.geo:
            self.location = str(data.youtube.geo.location())
        if data.youtube.rating:
            self.rating = data.youtube.rating.average

        # THUMBNAILS
        # drop previous thumbnails
        Link.objects.filter(src=self.resource, rel="thumbnail").delete()

        # link new
        for thumb in data.youtube.media.thumbnail:
            thumbres, status = getResource(thumb.url, download=True)
            if thumbres:
                Link.objects.get_or_create(src=self.resource, \
                        rel="thumbnail", dest=thumbres)

        # update resource.rtype
        if self.resource.rtype != "audio/video":
            self.resource.rtype = "audio/video"
            self.resource.save()

        # CREATE / UPDATE METADATA ANNOTATION
        (annotation, created) = Annotation.objects.get_or_create(\
                resource=self.resource, source="youtube")
        # clear old sections
        annotation.sections.all().delete()
        section = Section(annotation=annotation)
        text = "*YouTube Video*\n"
        if self.duration:
            text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
        if self.title:
            text += "|Title|%s|\n" % self.title
        if self.description:
            text += "|Description|%s|\n" % self.description
        if self.published:
            text += "|Published|%s|\n" % self.published
        if self.category:
            text += "|Category|%s|\n" % self.category
        if self.tags:
            text += "|Tags|%s|\n" % self.tags
        if self.view_count:
            text += "|View count|%s|\n" % self.view_count
        if self.rating:
            text += "|Rating|%s|\n" % self.rating
        if self.location:
            text += "|Location|%s|\n" % self.location

        section.source = text
        section.save()

    def thumbnail_source(self):
        thumbs = self.resource.thumbnails()
        if thumbs:
            return thumbs[-1].thumbnail_source()

    def extra_attributes(self):
        return """data-youtube-id="%s" """ % self.youtube_id

    def delegate(self):
        return "youtube"

    def name(self):
        if (self.title):
            return self.title


class YouTubeSniffer (ResourceSniffer):
    def sniff(self, data):
        m = youtubevid_pat.match(data.url)
        if m:
            yid = m.groupdict()['id']
            data.youtube_id = yid
            yt_service = gdata.youtube.service.YouTubeService()
            data.youtube = yt_service.GetYouTubeVideoEntry(video_id=yid)
            return YouTubeVideo
