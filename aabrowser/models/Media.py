# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


import aabrowser.FFMpeg
import eyeD3
import os
import subprocess
import sys
from aabrowser.templatetags.aamarkup import _humanize_seconds, _humanize_bytes
from Annotation import Annotation, Section
from django.conf import settings
from django.contrib import admin
from django.db import models
from Resource import Resource, ResourceSniffer


def calcBitRate(size, duration):
    return float(size * 8) / duration


def humanizeBitRate(rate):
    kbps = float(rate) / 1000
    return "%0.0f kbps" % kbps


class Video (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource", )

    resource = models.ForeignKey(
            Resource,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )
    width = models.PositiveIntegerField(
            null=True,
            blank=True
    )
    height = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    audio = models.BooleanField(
            default=False,
    )
    video_format = models.CharField(
            max_length=255,
            blank=True,
    )
    audio_format = models.CharField(
            max_length=255,
            blank=True,
    )
    fps = models.FloatField(
            null=True,
            blank=True,
    )

    def delegate(self):
        videotype = self.video_format.split(",")[0]
        if videotype in ("theora", "webm") \
                or self.resource.content_type.endswith("/ogg"):
            return "html5"
        elif videotype == "mpeg4":
            return "flowplayer"
        elif videotype == "flv":
            return "flowplayer"
        else:
            return "totem"

    def syncRequest(self, data):
        self.duration = data.ffmpeg.get("duration")
        self.video_format = data.ffmpeg.get("video")
        self.width = data.ffmpeg.get("width")
        self.height = data.ffmpeg.get("height")
        self.fps = data.ffmpeg.get("fps")
        if "audio" in data.ffmpeg:
            self.audio = True
            self.audio_format = data.ffmpeg.get("audio")
        else:
            self.audio = False
            self.audio_format = ""

        # update resource.rtype
        if self.audio:
            rtype = "audio/video"
        else:
            rtype = "video"
        if self.resource.rtype != rtype:
            self.resource.rtype = rtype
            self.resource.save()

        self.save()

        # CREATE / UPDATE METADATA ANNOTATION
        (annotation, created) = Annotation.objects\
                .get_or_create(resource=self.resource, source="mp3audio")
        # clear old sections
        annotation.sections.all().delete()
        section = Section(annotation=annotation)
        text = "*Video*\n"
        text += "|File size|%s|\n" \
                % _humanize_bytes(self.resource.content_length)
        if self.duration:
            text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
            bitrate = humanizeBitRate(calcBitRate(\
                    self.resource.content_length, self.duration))
            text += "|Bit rate|%s|\n" % bitrate
        if self.width and self.height:
            text += "|Size|%dx%d|\n" % (self.width, self.height)
        if self.video_format:
            text += "|Video format|%s|\n" % self.video_format
        if self.fps:
            text += "|Framerate|%0.1f fps|\n" % self.fps
        if self.audio_format:
            text += "|Audio format|%s|\n" % self.audio_format
        section.source = text
        section.save()

    def syncFile(self):
        if self.resource.isDownloaded():
            cpath = os.path.join(settings.MEDIA_ROOT, \
                    self.resource.cacheFile())

    def bitrate_display(self):
        if self.duration and self.resource.content_length:
            return humanizeBitRate(calcBitRate(self.resource.content_length, \
                    self.duration))
        else:
            return ""
    bitrate_display.short_description = "bitrate"

    def thumbnail_source(self):
        thumbs = self.resource.thumbnails()
        if thumbs:
            return thumbs[-1].thumbnail_source()
        else:
            poster = os.path.join(self.resource.cacheFolder(), "poster.jpg")
            poster_fp = os.path.join(settings.MEDIA_ROOT, poster)
            if os.path.exists(poster_fp):
                return poster
            # try to use FFMpeg to make a poster
            l = self.resource.getLocal()
            if l:
                src = os.path.join(settings.MEDIA_ROOT, l)
            else:
                src = self.resource.url
            # print "src", src
            # time = self.duration / 10
            tdata = aabrowser.FFMpeg.getThumbnail(src, size="320x320", \
                    time=10.0)
            # print len(tdata)
            self.resource.ensureCachePath()
            try:
                fout = open(poster_fp, "wb")
                fout.write(tdata)
                fout.close()
                return poster
            except IOError:
                # why does this happen?
                return

    def get_frame_thumb(self, time=0.0):
        cf = self.resource.cacheFolder()
        poster = os.path.join(cf, "frame%010.3f.jpg" % time)
        poster_fp = os.path.join(settings.MEDIA_ROOT, poster)
        if os.path.exists(poster_fp):
            return poster

        l = self.resource.getLocal()
        if l:
            src = os.path.join(settings.MEDIA_ROOT, l)
        else:
            src = self.resource.url
        tdata = aabrowser.FFMpeg.getThumbnail(src, size="320x320", time=time)
        self.resource.ensureCachePath()
        try:
            fout = open(poster_fp, "wb")
            fout.write(tdata)
            fout.close()
            return poster
        except IOError:
            # why does this happen?
            return


class Audio (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource", )

    resource = models.ForeignKey(
            Resource,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )
    audio_format = models.CharField(
            max_length=255,
            blank=True,
    )

    def syncRequest(self, data):
        self.duration = data.ffmpeg.get("duration")
        self.audio_format = data.ffmpeg.get("audio")
        self.save()
        # update resource.rtype
        if self.resource.rtype != "audio":
            self.resource.rtype = "audio"
            self.resource.save()

    def syncFile(self):
        if self.resource.isDownloaded():
            cpath = os.path.join(settings.MEDIA_ROOT, \
                    self.resource.cacheFile())

    def bitrate_display(self):
        if self.duration and self.resource.content_length:
            return humanizeBitRate(calcBitRate(self.resource.content_length, \
                    self.duration))
        else:
            return ""
    bitrate_display.short_description = "bitrate"


class Mp3Audio (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource", )

    resource = models.ForeignKey(
            Resource,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )
    audio_format = models.CharField(
            max_length=255,
            blank=True,
    )
    title = models.CharField(
            max_length=255,
            blank=True,
    )
    artist = models.CharField(
            max_length=255,
            blank=True,
    )
    album = models.CharField(
            max_length=255,
            blank=True,
    )
    year = models.CharField(
            max_length=255,
            blank=True,
    )
    tracknum = models.CharField(
            max_length=255,
            blank=True,
    )
    discnum = models.CharField(
            max_length=255,
            blank=True,
    )
    genre = models.CharField(
            max_length=255,
            blank=True,
    )

    def syncRequest(self, data):
        self.duration = data.ffmpeg.get("duration")
        self.audio_format = data.ffmpeg.get("audio")
        self.save()
        # update resource.rtype
        if self.resource.rtype != "audio":
            self.resource.rtype = "audio"
            self.resource.save()

        # CREATE / UPDATE METADATA ANNOTATION
        (annotation, created) = Annotation.objects.get_or_create(\
                resource=self.resource, source="mp3audio")
        # clear old sections
        annotation.sections.all().delete()
        section = Section(annotation=annotation)
        text = "*MP3 Audio*\n"
        text += "|File size|%s|\n" \
                % _humanize_bytes(self.resource.content_length)
        text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
        bitrate = humanizeBitRate(calcBitRate(self.resource.content_length, \
                self.duration))
        text += "|Bit rate|%s|\n" % bitrate
        if self.audio_format:
            text += "|Audio format|%s|\n" % self.audio_format
        section.source = text
        section.save()

    def syncFile(self):
        if self.resource.isDownloaded():
            cpath = os.path.join(settings.MEDIA_ROOT, \
                    self.resource.cacheFile())
            tag = eyeD3.Tag()
            tag.link(cpath)
            self.title = tag.getTitle() or ""
            self.artist = tag.getArtist() or ""
            self.album = tag.getAlbum() or ""
            self.year = tag.getYear() or ""
            self.genre = tag.getGenre() or ""
            self.tracknum = tag.getTrackNum() or ""
            self.discnum = tag.getDiscNum() or ""
            self.save()

            # CREATE / UPDATE METADATA ANNOTATION
            (annotation, created) = Annotation.objects.get_or_create(\
                    resource=self.resource, source="mp3audio")
            # clear old sections
            annotation.sections.all().delete()
            section = Section(annotation=annotation)
            text = "*MP3 Audio*\n"
            text += "|File size|%s|\n" \
                    % _humanize_bytes(self.resource.content_length)
            text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
            bitrate = humanizeBitRate(calcBitRate(\
                    self.resource.content_length, self.duration))
            text += "|Bit rate|%s|\n" % bitrate
            if self.audio_format:
                text += "|Audio format|%s|\n" % self.audio_format
            if self.title:
                text += "|Title|%s|\n" % self.title
            if self.artist:
                text += "|Artist|%s|\n" % self.artist
            if self.album:
                text += "|Album|%s|\n" % self.album
            if self.year:
                text += "|Year|%s|\n" % self.year
            if self.genre:
                text += "|Genre|%s|\n" % self.genre
            section.source = text
            section.save()

    def bitrate_display(self):
        if self.duration and self.resource.content_length:
            return humanizeBitRate(calcBitRate(self.resource.content_length, \
                    self.duration))
        else:
            return ""
    bitrate_display.short_description = "bitrate"

    def delegate(self):
        return "flowplayer"


class OggAudio (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource",)

    resource = models.ForeignKey(
            Resource,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )
    audio_format = models.CharField(
            max_length=255,
            blank=True,
    )
    title = models.CharField(
            max_length=255,
            blank=True,
    )
    artist = models.CharField(
            max_length=255,
            blank=True,
    )
    album = models.CharField(
            max_length=255,
            blank=True,
    )
    year = models.CharField(
            max_length=255,
            blank=True,
    )
    tracknum = models.CharField(
            max_length=255,
            blank=True,
    )
    discnum = models.CharField(
            max_length=255,
            blank=True,
    )
    genre = models.CharField(
            max_length=255,
            blank=True,
    )

    def syncRequest(self, data):
        self.duration = data.ffmpeg.get("duration")
        self.audio_format = data.ffmpeg.get("audio")
        self.save()
        # update resource.rtype
        if self.resource.rtype != "audio":
            self.resource.rtype = "audio"
            self.resource.save()

        # CREATE / UPDATE METADATA ANNOTATION
        (annotation, created) = Annotation.objects.get_or_create(\
                resource=self.resource, source="mp3audio")
        # clear old sections
        annotation.sections.all().delete()
        section = Section(annotation=annotation)
        text = "*OGG Audio*\n"
        text += "|File size|%s|\n" \
                % _humanize_bytes(self.resource.content_length)
        text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
        bitrate = humanizeBitRate(calcBitRate(self.resource.content_length, \
                self.duration))
        text += "|Bit rate|%s|\n" % bitrate
        if self.audio_format:
            text += "|Audio format|%s|\n" % self.audio_format
        section.source = text
        section.save()

    def syncFile(self):
        if self.resource.isDownloaded():
            pass

    def bitrate_display(self):
        if self.duration and self.resource.content_length:
            return humanizeBitRate(calcBitRate(self.resource.content_length, \
                    self.duration))
        else:
            return ""
    bitrate_display.short_description = "bitrate"

    def delegate(self):
        return "html5audio"

###############
# Sniffer

MEDIA_CONTENT_TYPES = (
    "audio/mpeg",
    "video/mp4",
    "application/ogg",
    "audio/ogg",
    "video/ogg",
)
MEDIA_EXTENSIONS = (
    ".flv",
    ".dv",
    ".mov",
    ".avi",
)


class MediaSniffer (ResourceSniffer):
    def sniff(self, data):
        print >> sys.stderr, "in MediaSniffer", data.ext
        if data.content_type in MEDIA_CONTENT_TYPES \
                or data.ext in MEDIA_EXTENSIONS:
            # use FFMpeg to look more closely...
            data.ffmpeg = aabrowser.FFMpeg.getStreamInfo(data.url)
            print >> sys.stderr, "checking ffmpeg info", data.ffmpeg
            if "video" in data.ffmpeg:
                print >> sys.stderr, "returning video"
                return Video
            elif "audio" in data.ffmpeg:
                if data.ffmpeg.get("audio").startswith("mp3"):
                    return Mp3Audio
                elif data.content_type.endswith("/ogg"):
                    return OggAudio
                else:
                    return Audio
