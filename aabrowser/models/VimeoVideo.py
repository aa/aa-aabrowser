# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


import aabrowser.iso8601
import gdata.youtube.service
import re
import urllib2
try:
    import simplejson as json
except:
    import json
from aabrowser.templatetags.aamarkup import _humanize_seconds, _humanize_bytes
from Annotation import Annotation, Section
from django.contrib import admin
from django.db import models
from Link import Link
from Resource import Resource, ResourceSniffer, getResource


vimeovid_pat = re.compile(r"^http://(www\.)?vimeo\.com/(?P<id>[\d]+)", re.I)


def doRequest(url):
    request = urllib2.Request(url)
    user_agent = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.14) "\
            + "Gecko/20080418 Ubuntu/7.10 (gutsy) Firefox/2.0.0.14"
    request.add_header("User-Agent", user_agent)
    rfile = urllib2.urlopen(request)
    data = rfile.read()
    rfile.close()
    return data


def getData(id):
    url = """http://vimeo.com/api/v2/video/%s.json""" % id
    data = doRequest(url)
    data = json.loads(data)
    return data[0]


class VimeoVideo (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource", )

    resource = models.ForeignKey(
            Resource,
    )
    vimeo_id = models.CharField(
            max_length=255,
            blank=True,
    )
    duration = models.FloatField(
            null=True,
            blank=True,
    )

    title = models.CharField(
            max_length=255,
            blank=True,
    )
    description = models.CharField(
            max_length=255,
            blank=True,
    )
    width = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    height = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    tags = models.CharField(
            max_length=255,
            blank=True,
    )
    upload_date_raw = models.CharField(
            max_length=255,
            blank=True,
    )
    upload_date = models.DateTimeField(
            null=True,
            blank=True,
    )
    thumbnail_large = models.CharField(
            max_length=255,
            blank=True,
    )
    thumbnail_medium = models.CharField(
            max_length=255,
            blank=True,
    )
    thumbnail_small = models.CharField(
            max_length=255,
            blank=True,
    )
    user_name = models.CharField(
            max_length=255,
            blank=True,
    )
    user_url = models.CharField(
            max_length=255,
            blank=True,
    )
    stats_number_of_comments = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    stats_number_of_likes = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    stats_number_of_plays = models.PositiveIntegerField(
            null=True,
            blank=True,
    )

    def syncRequest(self, data):
        self.vimeo_id = data.vimeo_id
        self.title = data.vimeo.get("title", "")
        self.description = data.vimeo.get("description", "")
        self.width = int(data.vimeo.get("width"))
        self.height = int(data.vimeo.get("height"))
        self.duration = float(data.vimeo.get("duration"))
        self.tags = data.vimeo.get("tags", "")
        self.upload_date_raw = data.vimeo.get("upload_date", "")
        if self.upload_date_raw:
            self.upload_date = aabrowser.iso8601\
                    .parse_date(self.upload_date_raw)
        else:
            self.upload_date = None
        self.thumbnail_large = data.vimeo.get("thumbnail_large", "")
        self.thumbnail_medium = data.vimeo.get("thumbnail_medium", "")
        self.thumbnail_small = data.vimeo.get("thumbnail_small", "")
        self.user_name = data.vimeo.get("user_name", "")
        self.user_url = data.vimeo.get("user_url", "")
        try:
            self.stats_number_of_comments = int(\
                    data.vimeo.get("stats_number_of_comments"))
        except ValueError:
            self.stats_number_of_comments = None
        try:
            self.stats_number_of_likes = int(\
                    data.vimeo.get("stats_number_of_likes"))
        except ValueError:
            self.stats_number_of_likes = None
        try:
            self.stats_number_of_plays = int(\
                    data.vimeo.get("stats_number_of_plays"))
        except ValueError:
            self.stats_number_of_plays = None

        # THUMBNAILS
        # drop previous thumbnails
        Link.objects.filter(src=self.resource, rel="thumbnail").delete()
        # & link up the new ones
        for url in (self.thumbnail_small, self.thumbnail_medium, \
                self.thumbnail_large):
            if url:
                thumbres, status = getResource(url)
                if thumbres:
                    Link.objects.get_or_create(src=self.resource, \
                            rel="thumbnail", dest=thumbres)

        # update resource.rtype
        if self.resource.rtype != "audio/video":
            self.resource.rtype = "audio/video"
            self.resource.save()

        self.save()

        # CREATE / UPDATE METADATA ANNOTATION
        (annotation, created) = Annotation.objects.get_or_create(\
                resource=self.resource, source="youtube")
        # clear old sections
        annotation.sections.all().delete()
        section = Section(annotation=annotation)
        text = "*Vimeo Video*\n"
        if self.duration:
            text += "|Duration|%s|\n" % _humanize_seconds(self.duration)
        if self.width and self.height:
            text += "|Size|%dx%d|\n" % (self.width, self.height)
        if self.title:
            text += "|Title|%s|\n" % self.title
        if self.description:
            text += "|Description|%s|\n" % self.description
        if self.upload_date:
            text += "|Upload date|%s|\n" % self.upload_date
        if self.tags:
            text += "|Tags|%s|\n" % self.tags
        if self.stats_number_of_plays:
            text += "|Number of plays|%s|\n" % self.stats_number_of_plays
        if self.stats_number_of_likes:
            text += "|Number of likes|%s|\n" % self.stats_number_of_likes
        if self.stats_number_of_comments:
            text += "|Number of comments|%s|\n" % self.stats_number_of_comments

        section.source = text
        section.save()

    def thumbnail_source(self):
        thumbs = self.resource.thumbnails()
        if thumbs:
            return thumbs[-1].thumbnail_source()

    def extra_attributes(self):
        return """data-vimeo-id="%s" """ % self.vimeo_id

    def delegate(self):
        return "vimeo"


class VimeoVideoSniffer (ResourceSniffer):
    def sniff(self, data):
        m = vimeovid_pat.match(data.url)
        if m:
            vid = m.groupdict()['id']
            data.vimeo_id = vid
            data.vimeo = getData(vid)
            return VimeoVideo
