# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


import datetime
import mimetypes
import os
import re
import urllib
import urllib2
import urlparse
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from ResourceOpener import ResourceOpener


# look up a resource by URL
# reload/refresh/check a resource
# would like ability to do sniffing without necessarily involving models
urlescape = re.compile("%[A-F0-9][A-F0-9]", re.I)


def getResource(url, check=False, reload=False, download=False):
    """
    default (no options):
      only use network if resource is not in db
      if resource is in database, do nothing (return existing resource) -- no
      network activity
      else: get/sniff/create
    check:
      conditional get: in case of 304, only updates last_contact timestamp
    reload:
      same as get: check resource, don't accept 304
    download:
      same as get + attempts to download file
    """

    # Returns Resource, StatusCode
    #   status = None:  check==False and resource exists
    #   Resource may be None if resource is unknown & a connection error occurs

    if reload:
        check = True  # reload implies check
    if download:
        check = True  # download implies check
        reload = True

    try:
        res = Resource.objects.get(url=url)
        # Resource exists
        if not check:
            return res, None
        try:
            data = ResourceOpener(url)
            if reload:
                data.get()  # prevent a 304
            else:
                data.get(res.last_modified_raw, res.etag)

            # print data, data.status

            if data.status == 304:
                # if DEBUG:
                #     print "Resource.getURL: 304 - returning resource as is"
                # Resource is unchanged, return immediately
                res.last_contact = datetime.datetime.now()
                res.save()
                return res, data.status

            contentcls = classFromRequest(data)
            content = res.content

            # print "contentcls", contentcls, content
            # print "url: %s, content: %s, contentcls: %s" % (url, str(content), str(contentcls))
            if (content and content.__class__ != contentcls) \
                    or (not content and contentcls):
                    # CLASS HAS CHANGED
                    res.content = None
                    if content:
                        content.delete()
                    if contentcls:
                        res.content = instantiateResourceClass(contentcls, \
                                data, res)
                        res.save()
            else:
                # Update the resource (no change to class)
                res.syncRequest(data)

        # In error cases, return the known resource (untouched), plus the error
        # code
        except urllib2.HTTPError, e:
            return res, e.code
        except urllib2.URLError, e:
            return res, e.reason

    except Resource.DoesNotExist:
        # NEW RESOURCE
        try:
            data = ResourceOpener(url)
            data.get()
            res = Resource.fromRequest(data)
            contentclass = classFromRequest(data)
            if contentclass:
                res.content = instantiateResourceClass(contentclass, data, res)
                res.save()

        except urllib2.HTTPError, e:
            return None, e.code
        except urllib2.URLError, e:
            return None, e.reason

    if res and download:
        res.download()

    return res, data.status


def classFromRequest(data):
    """
    Maps the contents of a URL to a ResourceContent class

    Returns: subclass of ResourceContent
    Modifies: data

    Performs a depth first search (down the class hierarchy of ResourceSniffer)
    to determine the best candidate for instantiating the Resource.  Calls
    "sniff" method of subclasses, when a subclass returns True: sniff will be
    called for each of it's subclasses The last class to have returned True
    wins The data dictionary allows detection data to be accumulated and
    possibly shared by the subclasses to do efficient detecting and/or eventual
    instantiation.
    """

    # DEPTH FIRST SEARCH OF SUBCLASSES TO FIND MOST SPECIFIC MATCHING
    # SUBCLASS...
    sniffcls = ResourceSniffer
    klass = None
    while True:
        oldsniffcls = sniffcls
        for subsniffcls in sniffcls.__subclasses__():
            k = subsniffcls().sniff(data)
            if k:
                klass = k
                sniffcls = subsniffcls
                break
        if sniffcls == oldsniffcls:
            break
    return klass

#################################
# ResourceSniffer
#
# Subclasses of ResourceSniffer allow mappings to specific Resource "content"
# models
#


def instantiateResourceClass(klass, data, resource):
    ret = klass(resource=resource)
    ret.syncRequest(data)
    ret.save()
    return ret


class ResourceSniffer(object):
    def sniff(cls, data):
        """ should return a model class if it fits """
        return None

#################################
# Resource

RESOURCE_TYPES = (
    ('audio', 'audio'),
    ('video', 'video (no audio)'),
    ('audio/video', 'video'),
    ('image', 'image'),
    ('html', 'html'),
    ('text', 'text'),
    ('', '')
)

RESOURCE_STATUS = (
    ('active', 'active'),
    ('default', 'default'),
    ('inactive', 'inactive')
)


class Resource(models.Model):
    """
    has understanding of local file / cacheability
    calls syncWithFile on content classes as appropriate
    """

    @classmethod
    def fromRequest(cls, data):
        ret = Resource(url=data.url)
        ret.syncRequest(data)
        return ret

    class Meta:
        app_label = "aabrowser"

    status = models.CharField(
            max_length=16,
            choices=RESOURCE_STATUS,
            default="default",
    )
    url = models.URLField(
            verify_exists=False,
    )
    filename = models.CharField(
            max_length=255,
            blank=True,
    )
    hostname = models.CharField(
            max_length=255,
            blank=True,
    )
    content_type = models.CharField(
            max_length=255,
            default="",
            blank=True,
    )
    content_length = models.IntegerField(
            default=0,
    )
    charset = models.CharField(
            max_length=64,
            default="",
            blank=True,
    )
    last_modified_raw = models.CharField(
            max_length=255,
            default="",
            blank=True,
    )
    last_modified = models.DateTimeField(
            null=True,
            blank=True,
    )
    etag = models.CharField(
            max_length=255,
            default="",
            blank=True,
    )
    object_type = models.ForeignKey(
            ContentType,
            null=True,
            blank=True,
    )
    object_id = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    content = generic.GenericForeignKey(
            'object_type',
            'object_id',
    )
    last_contact = models.DateTimeField(
            null=True,
            blank=True,
    )
    last_downloaded = models.DateTimeField(
            null=True,
            blank=True,
    )
    rtype = models.CharField(
            max_length=255,
            choices=RESOURCE_TYPES,
            blank=True,
    )
    top = models.IntegerField(
            default=0,
    )
    left = models.IntegerField(
            default=0,
    )
    width = models.IntegerField(
            default=320,
    )
    height = models.IntegerField(
            default=320,
    )
    db_creator = models.ForeignKey(
            User,
            null=True,
            blank=True,
            related_name="resources_as_creator",
    )
    db_created = models.DateTimeField(
            auto_now_add=True,
    )
    db_lastmodifier = models.ForeignKey(
            User,
            null=True,
            blank=True,
            related_name="resources_as_lastmodifier",
    )
    db_lastmodified = models.DateTimeField(
            auto_now=True,
    )

    def __unicode__(self):
        return self.url

    @models.permalink
    def get_absolute_url(self):
        return ('aa-resource', (str(self.id),))

    def syncRequest(self, data):
        parts = urlparse.urlparse(self.url)
        (_, self.filename) = os.path.split(parts.path)
        self.hostname = parts.hostname
        self.content_type = data.content_type
        self.charset = data.charset
        self.content_length = data.content_length
        self.etag = data.etag
        self.last_modified_raw = data.last_modified_raw
        self.last_modified = data.last_modified
        self.last_contact = datetime.datetime.now()
        self.save()

        # pass this on to the content object!
        content = self.content
        if content:
            content.syncRequest(data)

            # DELETE THE THUMBNAIL IF ZERO LENGTH
            tp = content.thumbnail_source()
            if tp:
                tp = os.path.join(settings.MEDIA_ROOT, tp)
                if os.path.exists(tp):
                    stat = os.stat(tp)
                    if stat.st_size == 0:
                        os.unlink(tp)
                        # try again
                        tp = content.thumbnail_source()

    def cacheFolder(self):
        urlparts = urlparse.urlparse(self.url)
        path = os.path.join(urlparts.netloc, urlparts.path.lstrip('/'))
        if urlparts.query:
            path = os.path.join(path, "?" + urlparts.query)
        # FOR NOW I AVOID ISSUES WITH "%20" in URLs by replacing... not ideal
        path = urlescape.sub("_", path)
        path = os.path.join("cache", path)
        # abspath = os.path.join(settings.MEDIA_ROOT, path)
        return path

    def localExtension(self):
        """
        use original extension or guess based on content-type
        """
        path = urlparse.urlparse(self.url).path
        (base, ext) = os.path.splitext(path)
        ext = ext.lower()
        if ext:
            return ext
        guesses = mimetypes.guess_all_extensions(self.content_type)
        if guesses:
            return guesses[0]
        return ""

    def cacheFile(self):
        return os.path.join(self.cacheFolder(), \
                "original" + self.localExtension())

    def cacheFileIfExists(self):
        path = self.cacheFile()
        fp = os.path.join(settings.MEDIA_ROOT, path)
        if os.path.exists(fp):
            return path

    def ensureCachePath(self):
        cpath = os.path.join(settings.MEDIA_ROOT, self.cacheFile())
        (folder, base) = os.path.split(cpath)
        try:
            os.makedirs(folder)
        except OSError:
            pass

    def cache_url(self):
        if self.isDownloaded():
            return settings.MEDIA_URL + self.cacheFile()
        else:
            return self.url

    def download(self, verbose=False):
        cpath = os.path.join(settings.MEDIA_ROOT, self.cacheFile())
        # (folder, base) = os.path.split(cpath)
        self.ensureCachePath()
        try:
            outfile = open(cpath, "wb")
            bytes = ResourceOpener(self.url).writeToFile(outfile)
            if bytes > 0:
                self.last_downloaded = datetime.datetime.now()
                self.save()
                content = self.content
                if content and hasattr(content, "syncFile"):
                    content.syncFile()
                return True
            else:
                return False
        except IOError:
            # this is sometimes failing on complicated URLs
            return False

    def isDownloaded(self):
        cpath = os.path.join(settings.MEDIA_ROOT, self.cacheFile())
        if os.path.isfile(cpath):
            if self.content_length:
                fileinfo = os.stat(cpath)
                if fileinfo.st_size > 0:
                    return True
#                ret = fileinfo.st_size == self.content_length
#                return ret
            else:
                return True
        return False
    isDownloaded.boolean = True
    isDownloaded.short_description = "Downloaded"

    # MM, adding Jan 6 2011 to allow URLs within Django MEDIA_ROOT to be
    # recognized / handled as local file

    def getLocal(self):
        """ return path of file, relative to MEDIA_ROOT, if it exists. this
        could be a cache file or the original in the case of a URL of a local
        resource """
        if self.url.startswith(settings.MEDIA_URL):
            return self.url[len(settings.MEDIA_URL):].lstrip('/')
        elif self.isDownloaded():
            return self.cacheFile()

    def thumbnail_source(self):
        content = self.content
        try:
            return content.thumbnail_source()
        except AttributeError:
            return
        except OSError:
            return

    def thumbnails(self):
        """ return list of Image objects sorted by width """
        thumbs = []
        for link in self.links_as_src.filter(rel="thumbnail"):
            thumbs.append(link.dest.content)
        thumbs = [(x.width, x) for x in thumbs]
        thumbs.sort()
        thumbs = [x[1] for x in thumbs]
        return thumbs

    def rtype_class(self):
        return "rtype_" + self.rtype.replace("/", "")

    def content_class_name(self):
        content = self.content
        if content:
            return content.__class__.__name__

    def url_display(self):
        ret = self.url.replace("/", "/&shy;")
        ret = urllib.unquote(ret)
        return ret
    url_display.allow_tags = True

    def duration(self):
        try:
            d = self.content.duration
        except AttributeError:
            return
        return d

    def content_delegate(self):
        try:
            return self.content.delegate()
        except AttributeError:
            return

    def extra_attributes(self):
        try:
            return self.content.extra_attributes()
        except AttributeError:
            return ""

    def delete_cached_files(self):
        cf = os.path.join(settings.MEDIA_ROOT, self.cacheFolder())
        if os.path.exists(cf):
            files = os.listdir(cf)
            for f in files:
                fp = os.path.join(cf, f)
                os.unlink(fp)
            os.removedirs(cf)

    def name(self):
        try:
            d = self.content.name()
        except AttributeError:
            ret = self.filename
            ret = ret.replace("%20", " ")
            ret = ret.replace("_", " ")
            return ret
        return d
