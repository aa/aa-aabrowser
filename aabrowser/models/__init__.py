from Resource import Resource, getResource
from Link import Link
from Annotation import Annotation, Section, Tag, SectionTag

#from VideoResource import VideoResource
#from OggResource import OggResource
from Image import Image
from Media import Video, Audio, Mp3Audio
#from HTMLResource import HTMLResource
#from MediaWikiResource import MediaWikiResource, MediaWikiCategory
from VimeoVideo import VimeoVideo
from YouTubeVideo import YouTubeVideo
