# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.

from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from Resource import Resource, getResource
from aabrowser.LinkForm import name_urlescape
from aabrowser.LinkForm import Link_parseText, name_normalize, name_urlunescape
import aabrowser.timecode
import re

# from aabrowser import SRT
from aabrowser import ASRT


class Annotation (models.Model):
    """
    Defines an annotation of a Resource.
    An annotation contains one or more Section instances.
    """
    resource = models.ForeignKey(
            Resource,
            related_name="annotations",
            help_text="The annotated Resource",
    )
    source = models.CharField(
            max_length=255,
            blank=True,
            help_text="Resource type specific tag describing where the annotation comes from",
    )
    top = models.IntegerField(
            default=0,
            help_text="Annotation container Y offset",
    )
    left = models.IntegerField(
            default=0,
            help_text="Annotation container X offset",
    )
    width = models.IntegerField(
            default=240,
            help_text="Annotation container width",
    )
    height = models.IntegerField(
            default=240,
            help_text="Annotation container height",
    )
    db_creator = models.ForeignKey(
            User,
            null=True,
            blank=True,
            related_name="annotations_as_creator",
            help_text="The user who created the annotation",
    )
    db_created = models.DateTimeField(
            auto_now_add=True,
            help_text="Date and time of creation",
    )

    class Meta:
        app_label = "aabrowser"
        ordering = ("db_created", )

    def __unicode__(self):
        return self.resource.url

    def ssections(self):
        """
        Returns this annotation sections in chronological order.
        Forces the sections with a start field set to None to come first.
        """
        return self.sections.order_by("position", "start")

    def update_from_text(self, text, section_ids=[]):
        """
        Updates the Annotation with the given text.
        """
        # remove case of ['']
        for sid in section_ids:
            try:
                s = Section.objects.get(pk=sid)
                s.delete()
            except Section.DoesNotExist:
                pass

        if text:
            titles = ASRT.parse(text)

            if not titles:
                return

            url = self.resource.url

            # FIX END TIMES
            for (i, (th, tb)) in enumerate(titles):
                if (th['start'] != None) and (th['end'] == None):
                    if i + 1 < len(titles):
                        (nh, nb) = titles[i + 1]
                        cururl = th['url'] or url
                        nexturl = nh['url'] or url
                        if cururl == nexturl:
                            th['end'] = nh['start']

            for (i, (th, tb)) in enumerate(titles):
                section = Section(annotation=self)
                section.position = i + 1
                url = th['url'] or url
                (section.resource, status) = getResource(url)
                section.source = tb
                section.start = th['start']
                section.end = th['end']
                if 'hash' in th:
                    section.fragment = th['hash'] or ""
                section.save()

                ## process for tags
                for link in Link_parseText(tb):
                    rel = name_normalize(link.rel or "")
                    (tag, created) = Tag.objects\
                            .get_or_create(name=name_normalize(link.item))
                    SectionTag.objects.create(section=section, tag=tag, \
                            rel=rel)

    def get_text(self):
        ret = ""
        for section in self.ssections():
            if section.start != None:
                ret += aabrowser.timecode.timecode_fromsecs(section.start)
                ret += " --> "
                if section.end != None:
                    ret += aabrowser.timecode.timecode_fromsecs(section.end)
                ret += "\n"
            ret += section.source
            ret += "\n"
        return ret


class Section (models.Model):
    """
    Defines a section of an Annotation.
    """
    annotation = models.ForeignKey(
            Annotation,
            related_name="sections",
            help_text="The annotation this section belongs to.",
    )
    resource = models.ForeignKey(
            Resource,
            related_name="sections",
            null=True,
            blank=True,
    )
    position = models.IntegerField(
            null=True,
            blank=True,
            help_text="The sequence position / index of this section with relation to the annotation",
    )
    source = models.TextField(
            blank=True,
            help_text="Content",
    )
    start = models.FloatField(
            null=True,
            blank=True,
    )
    start_tc = models.CharField(
            max_length=255,
            blank=True,
    )
    end = models.FloatField(
            null=True,
            blank=True,
    )
    end_tc = models.CharField(
            max_length=255,
            blank=True,
    )
    fragment = models.TextField(
            blank=True,
    )
    db_creator = models.ForeignKey(
            User,
            null=True,
            blank=True,
            related_name="sections_as_creator",
    )
    db_created = models.DateTimeField(
            auto_now_add=True,
    )
    db_lastmodifier = models.ForeignKey(
            User,
            null=True,
            blank=True,
            related_name="sections_as_lastmodifier",
    )
    db_lastmodified = models.DateTimeField(
            auto_now=True,
    )

    tags = models.ManyToManyField(
            'Tag',
            through='SectionTag',
            related_name='sections',
    )

    def aresource(self):
        if self.resource:
            return self.resource
        return self.annotation.resource

    class Meta:
        app_label = "aabrowser"
        ordering = ("position", "start")

    def __unicode__(self):
        return u"Section (%s)" % self.annotation.resource.url

    def duration(self):
        """
        Returns the duration of the Section.
        """
        if (self.start != None and self.end != None):
            return (self.end - self.start)

    def thumbnail_source(self):
        if (self.start):
            try:
                return self.annotation.resource.content\
                        .get_frame_thumb(self.start + 1)
            except AttributeError:
                pass
        return self.annotation.resource.thumbnail_source()


class Tag(models.Model):
    """
    Defines a tag. It is generic, so it can be attached to any kind of object.
    Tags can have a parent tag or not:
        foo
          tag_1
          tag_3
          tag_4
        bar
          tag_2
          tag_5
        undefined:
          tag_6
    """
    name = models.CharField(
            max_length=255,
    )
    parent = models.ForeignKey(
            'self',
            null=True,
            blank=True,
            related_name="children",
    )
    text = models.TextField(
            blank=True,
    )
    object_type = models.ForeignKey(
            ContentType,
            null=True,
            blank=True,
    )
    object_id = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    content = generic.GenericForeignKey(
            'object_type',
            'object_id',
    )

    class Meta:
        app_label = "aabrowser"
        ordering = ("name", )

    def __unicode__(self):
        return self.name

    def urlname(self):
        """
        Returns this tag name formatted for being used in a URL.
        """
        return name_urlescape(self.name)

    def child_count(self):
        """
        Returns the hierarchy depth.
        """
        return self.children.count()

    def section_count(self):
        """
        Returns this tag occurence count.
        """
        return self.sections.count()

    @models.permalink
    def get_absolute_url(self):
        return ('aa-playlist', (self.urlname(),))

    def get_internal_url(self):
        return u"/tags/%s/" % self.urlname()


class SectionTag (models.Model):
    """
    Links a Tag and a section "through" a relation. In other words,
    it defines qualified tags. This objects are created in the annotatino
    sections using the MediaWiki syntax: [[ relation::tag ]]
    """
    section = models.ForeignKey(
            'Section',
    )
    tag = models.ForeignKey(
            'Tag',
    )
    rel = models.CharField(
            max_length=255,
            blank=True,
    )

    class Meta:
        app_label = "aabrowser"

    def __unicode__(self):
        return "%s (%s) %s" % (self.section, self.rel, self.tag)
