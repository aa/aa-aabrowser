# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


import aabrowser.FFMpeg
import os
import subprocess
import urllib
from django.conf import settings
from django.db import models
from Resource import Resource, ResourceSniffer


IMAGEFORMATS = (
    ('jpeg', 'JPEG'),
    ('png', 'PNG'),
    ('gif', 'GIF'),
    ('other', 'other')
)


def getExifData(path):
    output = subprocess.Popen([settings.EXIFTOOL, path], \
            stdout=subprocess.PIPE).communicate()[0]
    lines = output.strip().splitlines()
    ret = {}
    for line in lines:
        (key, value) = line.split(":", 1)
        key = key.strip()
        # if EXIF_EXCLUDE_KEYS.has_key(key): continue
        ret[key] = value.strip()
    return ret


class Image (models.Model):
    class Meta:
        app_label = "aabrowser"
        ordering = ("resource",)

    resource = models.ForeignKey(
            Resource,
    )
    width = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    height = models.PositiveIntegerField(
            null=True,
            blank=True,
    )
    format = models.CharField(
            max_length=16,
            choices=IMAGEFORMATS,
            default="other",
    )
    gray = models.BooleanField(
            default=False,
    )

    def syncRequest(self, data):
        # map content-type to format...
        (foo, bar) = data.content_type.split("/", 1)
        format = "other"
        for i in IMAGEFORMATS:
            if i[0] == bar:
                format = bar
                break
        if self.format != format:
            self.format = format

        ffmpeg = aabrowser.FFMpeg.getStreamInfo(self.resource.url)
        if 'width' in ffmpeg:
            self.width = ffmpeg['width']
        if 'height' in ffmpeg:
            self.height = ffmpeg['height']
        if 'video' in ffmpeg:
            self.gray = "gray" in ffmpeg['video'].lower()

        self.save()

        # update resource.rtype
        if self.resource.rtype != "image":
            self.resource.rtype = "image"
            self.resource.save()

    def syncFile(self):
        if self.resource.isDownloaded():
            cpath = os.path.join(settings.MEDIA_ROOT, \
                    self.resource.cacheFile())
            metadata = getExifData(cpath)
            save = False
            if "Image Width" in metadata:
                self.width = int(metadata.get("Image Width"))
                save = True
            if "Image Height" in metadata:
                self.height = int(metadata.get("Image Height"))
                save = True
            if save:
                self.save()
            return metadata

    def thumbnail_source(self):
        if not self.resource.isDownloaded():
            self.resource.download()
        return self.resource.cacheFileIfExists()


class ImageSniffer (ResourceSniffer):
    def sniff(self, data):
        if data.content_type.startswith("image/"):
            return Image
