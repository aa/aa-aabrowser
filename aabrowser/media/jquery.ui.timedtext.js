/* 
Requires:
jquery.timeline.js
*/

(function ($, undefined) {

function log () {
    try { console.log.apply(console, arguments); } catch (e) {}
}

function getEndTime (elt) {
    // looks in (1) self, (2) start of a subsequent sibling element, or (3) recurses to parents
    // look for end time first in self
    elt = $(elt);
    var ret = elt.attr("data-end");
    if (ret !== undefined) {
        // $.log("found in elt", ret);
        return $.timecode_tosecs_attr(ret);
    }
    // ... try a start of a sibling
    ret = $(elt).nextAll("[data-start]:first").attr("data-start");
    if (ret !== undefined) {
        // $.log("found in sibling", ret);
        return $.timecode_tosecs_attr(ret);
    }
    // ... finally from a parent (recursive)
    var p = elt.parent(); // jquery now seems to return the "document" as parent of the <html> which can cause havoc!
    if (p.length && p[0] !== document) {
        // $.log("recursing");
        return getEndTime(p);
    }
    /*
    else {
        $.log("no parent, stopping");
    } */
}

$.widget("aa.timedtext", {
	options: {
        implicitEndTimes: true
	},

    _create: function () {
        // log("create!");
		this.element.addClass( "aa-timedtext" );
        var o = this.options;
        this.refresh();
	},

    _init: function () {
        // log("init");
    },

    setCurrentTime: function (t) {
        // log("setCurrentTime", t);
        this.options._timeline.setCurrentTime(t);
    },

    refresh: function () {
        var that = this;
        this.options._timeline = $.timeline({
            show: function (elt) {
                $(elt).addClass("aa-timedtext-title-active");
                that._trigger("show", null, {elt: elt})
            },
            hide: function (elt) {
                $(elt).removeClass("aa-timedtext-title-active");
                that._trigger("hide", null, {elt: elt})
            }
        });
        var o = this.options;
        $("[data-start]", this.element).each(function () {
            $(this).addClass("aa-timedtext-title");
            var elt = $(this).get(0);
            var start = $.timecode_tosecs_attr($(this).attr("data-start"));
            var end = o.implicitEndTimes ? getEndTime(this) : $.timecode_tosecs_attr($(this).attr("data-end"));
            o._timeline.add(elt, start, end);
        });
    },

	destroy: function() {
	    // log("destroy");
		this.element.removeClass( "aa-timedtext" );
		$.Widget.prototype.destroy.apply( this, arguments );
	}

});

})(jQuery);

