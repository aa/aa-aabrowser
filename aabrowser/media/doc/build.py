#! /usr/bin/env python

import os.path
from django.template import Template, Context, loader
from django.conf import settings
import codecs


settings.configure()

DIRNAME = os.path.abspath(os.path.dirname(__file__))
settings.TEMPLATE_DIRS = (
    os.path.join(DIRNAME, "templates"),
)
settings.INSTALLED_APPS = (
    'django.contrib.markup',
)

f = codecs.open("templates/pages.lst", "r", 'utf-8')
pages = f.read().splitlines()
f.close()

for page in pages:
    f = codecs.open(os.path.join("templates", page), "r", 'utf-8')
    t = Template(f.read())
    c = Context({'filename': 'jquery.aa_timeline.js'})
    f.close()
    out = os.path.join("ok", os.path.split(page)[-1])
    f = codecs.open(out, "w", 'utf-8')
    f.write(t.render(c))
    f.close()

#print(t.render(c).encode('utf-8'))
