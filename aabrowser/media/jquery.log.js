(function ($) {
jQuery.log = function () {
	/* use Firebug console if available, fallback to a div with id log */
    var showindiv = true;
    // if ($.browser.mozilla) { // added to avoid falling for "fake" console objects
    // but this fails for chrome!
    try {
    	console.log.apply(console, arguments);
	    showindiv = false;
    } catch (e) {}
    // }
    if (showindiv) {
	    var msg = "";
	    for (var i=0; i<arguments.length; i++) msg += arguments[i] + (i+1<arguments.length?", ":"");
        var msgobj = $("<div></div>").addClass("logmsg").html(msg);
	    $("#log").prepend(msgobj);
	}
}
})(jQuery);
