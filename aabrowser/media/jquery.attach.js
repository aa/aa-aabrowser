(function ($) {

/*
Inserts "anchor" <div> elements into fromElement, into which placed elements are placed.

Requirements: fromElt needs to support nested elements (so instead of a raw <img>, use a <div> wrapper of some sort).
*/

$.fn.attach = function (fromElt, options) {
    fromElt = $(fromElt);

    if (typeof(options) === "string") {
        var optstr = options;
        options = {};
        options.from = optstr.substr(0, 2);
        options.to = optstr.substr(2, 2);
        options.spacing = optstr.substr(4);
    }

    var pos = fromElt.css("position");
    if (pos === "static") fromElt.css("position", "relative"); // ensure at least relative positioning
    if (jQuery.browser.msie) fromElt.css("zoom", 1); // force 'hasLayout' in IE

    function ensureAnchor (thing, h, v) {
        // attempt to use previously defined anchor
        var anchor = $(".cc_anchor"+v+h, thing);
        if (anchor.length) { return anchor; }
        
        css = {};
        if (h === "L") { css.left = "0"; } else { css.right = "0"; }
        if (v === "T") { css.top = "0"; } else { css.bottom = "0"; }
        anchor = $("<div></div>").addClass("cc_anchor").css({
            position: 'absolute'
        }).addClass("cc_anchor"+v+h).css(css).appendTo(thing);
        return anchor;
    }

    var fromHandle = ((options && options.from) || "TR").toUpperCase();
    var toHandle = ((options && options.to) || "TL").toUpperCase();

    // convenience names
    var from_v = fromHandle.substr(0, 1);
    var from_h = fromHandle.substr(1, 1);
    var to_v = toHandle.substr(0, 1);
    var to_h = toHandle.substr(1, 1);

    var anchor = ensureAnchor(fromElt, from_h, from_v);
    var spacing = (options && options.spacing) || 0;

    return this.each(
        function () {
            css = {};
            css.position = "absolute";
            /* use spacing only for non-matching edges (top to bottom, right to left) */
            if (to_v == "T") {
                css.top = (from_v === "T") ? "0" : spacing;
            } else {
                css.bottom = (from_v === "B") ? "0" : spacing;
            }
            if (to_h == "L") {
                css.left = (from_h === "L") ? "0" : spacing;
            } else {
                css.right = (from_h === "R") ? "0" : spacing;
            }
            $(this).appendTo(anchor).css(css);
        }
    );
};

})(jQuery);

