/**
 * Active Archives Playlist
 *
 * Copyright 2011, Active Archives Authors (http://activearchives.org/wiki/Authors)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 *
 * Depends:
 *   jquery.timecode.js
 *   jquery.timeline.js
 *   jquery.autoscrollable.js
 *   jquery.ui.widget.js VERSION >=1.8!

changelog

feb 27 2011
when entering edit mode, match the scroll position of the player.

**/

(function ($, undefined) {

DEBUG = false;

function log () {
    try { console.log.apply(console, arguments); } catch (e) {}
}

MODE_LABELS = ["S", "L"];

$.widget("aa.playlist", {
	options: {
	    edit: false, // initial edit mode
	    resource_url: null,
        annotation_id: null,
        implicitEndTimes: true,
        delete_url: "",
        size_url: "",
        srt_url: "",
        export_audacity_url: "",
        import_audacity_url: "",
        position_url: "",
        updatePositionURL: "",
        drop_on_cancel: false,
        mode: "list" // list or single
	},

    _create: function () {
        // log("create");
	},

    _init: function () {
        // log("aa.playlist init");
        var that = this;
		this.element.addClass( "aa-playlist" );
        var o = this.options;
        o.annotation_id = $(this.element).attr("data-id") || o.annotation_id;
        o._editing = null; // null initial state allows read or edit call to take at end of init
        o._playing = false; // when false, responds to timeupdate events by following
        // this.refresh();

        var playmenu = $(".aa-playlist-play-menu", this.element);
        var viewmenu, viewmenucontents, exportmenu, exportmenucontents;
        if (playmenu.length === 0) {
            playmenu = $("<div></div>").addClass("aa-playlist-play-menu").prependTo(this.element);
            o._playbutton = $("<a>play</a>").addClass("aa-button aa-playlist-play-button").appendTo(playmenu);
            $("<a>edit</a>").addClass("aa-button aa-playlist-edit-button").appendTo(playmenu);

            viewmenu = $("<div></div>").addClass("aa-playlist-view-menu").appendTo(playmenu);
            $("<a>view</a>").addClass("aa-button aa-playlist-view-button").appendTo(viewmenu);
            viewmenucontents = $("<div></div>").addClass("aa-playlist-menu-contents").appendTo(viewmenu);
            $("<a>active titles only</a>").addClass("aa-button aa-playlist-view-single-button").appendTo(viewmenucontents);
            $("<a>all titles (list)</a>").addClass("aa-button aa-playlist-view-list-button").appendTo(viewmenucontents);

            exportmenu = $("<div></div>").addClass("aa-playlist-export-menu").appendTo(playmenu);
            $("<a>im/export</a>").addClass("aa-button aa-playlist-export-button").appendTo(exportmenu);
            exportmenucontents = $("<div></div>").addClass("aa-playlist-menu-contents").appendTo(exportmenu);
            if (o.srt_url) {
                $("<a>Export SRT</a>").addClass("aa-button aa-playlist-export-srt-button").appendTo(exportmenucontents);
            }
            if (o.export_audacity_url) {
                $("<a>Export Audacity Labels</a>").addClass("aa-button aa-playlist-export-audacity-button").appendTo(exportmenucontents);
            }
            if (o.import_audacity_url) {
                $("<a>Import Audacity Labels</a>").addClass("aa-button aa-playlist-import-audacity-button").appendTo(exportmenucontents);
            }
            if (o.annotation_id) {
                $("<a>delete</a>").addClass("aa-button aa-playlist-delete-button").appendTo(playmenu);
            }
        } else {
            viewmenu = $(".aa-playlist-view-menu", playmenu);
            viewmenucontents = $(".aa-playlist-menu-contents", viewmenu);
            exportwmenu = $(".aa-playlist-export-menu", playmenu);
            exportmenucontents = $(".aa-playlist-menu-contents", exportmenu);
            o._playbutton = $(".aa-playlist-play-button", playmenu);
        };
        
        var viewmenuhider = $.delayedaction(function () { viewmenucontents.hide(); }, 250);
        $(".aa-playlist-view-button", playmenu).mouseover(function () {
            viewmenuhider.cancel();
            viewmenucontents.show();
            exportmenuhider.do_now();
        }).mouseleave(function () {
            viewmenuhider.do_soon();
        });
        viewmenucontents.mouseover(function () {
            viewmenuhider.cancel();
        }).mouseleave(function () {
            viewmenuhider.do_soon();
        });

        var exportmenuhider = $.delayedaction(function () { exportmenucontents.hide(); }, 250);
        $(".aa-playlist-export-button", playmenu).mouseover(function () {
            exportmenuhider.cancel();
            exportmenucontents.show();
            viewmenuhider.do_now();
        }).mouseleave(function () {
            exportmenuhider.do_soon();
        });
        exportmenucontents.mouseover(function () {
            exportmenuhider.cancel();
        }).mouseleave(function () {
            exportmenuhider.do_soon();
        });
        
        var playmenuhider = $.delayedaction(function () {
            playmenu.hide();
        }, 250);
        $(this.element).mouseover(function(){
            if (!o._editing) playmenu.show();
        }).mouseleave(function(){
            playmenuhider.do_soon();
        });
        o._playmenuhider = playmenuhider;
        
        var editmenu = $(".aa-playlist-edit-menu", this.element);
        if (editmenu.length === 0) {
            editmenu = $("<div></div>").addClass("aa-playlist-edit-menu").prependTo(this.element);
            $("<a>save</a>").addClass("aa-button aa-playlist-edit-save-button").appendTo(editmenu);
            $("<a>cancel</a>").addClass("aa-button aa-playlist-edit-cancel-button").appendTo(editmenu);
        };
        o._editmenu = editmenu;
        
        /* assign menu button functionality */

        o._playbutton.click(function () {
            that.play();
            // $("div.aa-timecodes:visible:first a.playlink", that.element).trigger("click");
        });
        
        $(".aa-playlist-view-single-button", playmenu).click(function () {
            that.set_single_mode();
        });
        $(".aa-playlist-view-list-button", playmenu).click(function () {
            that.set_list_mode();
        });

        $(".aa-playlist-delete-button", playmenu).click(function () {
            if (confirm("Are you sure?")) {
                /* DO DELETE ANNOTATION! */
                $.ajax({
                    type: "POST",
                    url: o.delete_url,
                    data: {
                        resource: o.resource_url
                    },
                    success: function () {
                        that.element.remove();
                    }
                });
            }
            return false;
        });

        $(".aa-playlist-export-srt-button", playmenu).click(function () {
            var url = o.srt_url;
            window.open(url, "extern", "menubar=yes,location=no,resizable=yes,scrollbars=yes,status=no");
            return false;
        });
        $(".aa-playlist-export-audacity-button", playmenu).click(function () {
            var url = o.export_audacity_url;
            window.open(url, "extern", "menubar=yes,location=no,resizable=yes,scrollbars=yes,status=no");
            return false;
        });
        $(".aa-playlist-import-audacity-button", playmenu).click(function () {
            var url = o.import_audacity_url;
            window.open(url, "extern", "menubar=yes,location=no,resizable=yes,scrollbars=yes,status=no");
            return false;
        });
        
        /* main [edit] button */
        $(".aa-playlist-edit-button", playmenu).click(function () {
            that.edit();
            return false;
        });

        /* section [edit] button */
        /* DISABLED UNTIL SERVER SIDE IS FIXED!...
        $(".aa-playlist-section", this.element).mouseover(function(){
            $('.aa-playlist-section-edit', this).show();
        });
        $(".aa-playlist-section", this.element).mouseout(function(){
            $('.aa-playlist-section-edit', this).hide();
        });
        */
        
        o._editSection = null;
        o._display = $("<div></div>").addClass("aa-playlist-display").appendTo(this.element);
        o._edit = $("<div></div>").addClass("aa-playlist-edit").appendTo(this.element);
        // var editbuttons = $("<div></div>").addClass("aa-playlist-edit-buttons"); // .appendTo(o._edit);

        /* LISTEN FOR AA PAGE EVENTS: pastetimecode  */
        // store refs to handlers to be able to clear them explicitly in destroy
        o._pastetimecodehandler = function (evt, ct) {
            // log("playlist: pastetimecode", ct);
            if (o._textarea_focus) {
                $.insertAtCaret(o._textarea.get(0), $.timecode_fromsecs(ct, true)+" -->", true);
            }
        }
        $("body").bind("pastetimecode", o._pastetimecodehandler);

        o._textarea = $("<textarea></textarea>").addClass("aa-playlist-textarea").focus(function (evt) {
            o._textarea_focus = true;
        }).blur(function () {
            o._textarea_focus = false;
        }).droppable({
            accept: "a.tagtool",
            drop: function (evt, ui) {
                // $.log("drop", this, ui, evt);
                var label = ui.helper.text();
                $.insertAtCaret(this, "[[" + label + "]]");
            }
        }).appendTo(o._edit);
        
        $(".aa-playlist-edit-save-button", editmenu).click(function () { that.save(); return false });
        $(".aa-playlist-edit-cancel-button", editmenu).click(function () {
            // $('div#textile-help').hide(); //hide textile help
            if (!o.annotation_id && o.drop_on_cancel) {
                that.element.remove();
            } else {
                that.read();
            }
        });
     
        // sections
        $(".aa-playlist-section", this.element).each(function () {
            $(this).appendTo(o._display);
            var sectionedit = $("<div></div>").addClass("aa-playlist-section-edit").prependTo(this);
            var thesection = this;
            var start = $(this).attr("data-start");
            var url = $(this).attr("data-url");
            var editurl = $(this).attr("data-edit-url");
            var end = $(this).attr("data-end");
            var player = function () {
                $(thesection).addClass("aa-playlist-section-active");
                o._playing = true;
                var singleton = true;
                var nextsection = $(thesection).nextAll(".aa-playlist-section:visible").first();
                if (nextsection.length) {
                    var nexturl = $(nextsection).attr("data-url");
                    var nextstart = $(nextsection).attr("data-start");
                    // not ready to use this, see todo below...
                    // if (url === nexturl && end === nextstart) singleton = false;
                }
                /*

                TODO: need support for a "soft callback" on ended (ie -- no pause, but keeps playing
                and then here a "soft start" of the next title where in fact only a new end time is set.

                This could be generalized by adding a "set cuepoint" type event on playable.
                
                */
                // single should be false when next title continues...
                if (singleton) {
                    that.element.trigger("playrequest", [url, start, end, function () {
                        $(thesection).removeClass("aa-playlist-section-active");
                        // var nextsection = $(thesection).nextAll(".aa-playlist-section:visible").first();
                        if (nextsection.length) {
                            // console.log("playlist.next", nextsection)
                            $("a.playlink", nextsection).trigger("click");
                        } else {
                            //  log("playlist: finished");
                            that.stop();
                            // o._playing = false;
                        }
                    }]);
                } else {
                    that.element.trigger("playrequest", [url, start]);
                }
                return false;
            }
            var playlink = $("<a>play</a>").attr("href", "#").addClass("playlink").click(player);
            // TIMECODES
            if (start !== undefined) {
                var timecode = $("<div></div>").addClass("aa-timecodes");
                start = parseFloat(start);
                $("<span></span>").addClass("aa-timecodes-start").text($.timecode_fromsecs(start)).click(function () {
                    if (o._playing) {
                        player();
                    } else {
                        that.element.trigger("playrequest", [url, start]);
                    }
                }).appendTo(timecode);
                $("<span></span>").addClass('aa-timecodes-arrow').text("-->").appendTo(timecode);
                if (end !== undefined) {
                    $("<span></span>").addClass("aa-timecodes-end").text($.timecode_fromsecs(end)).click(function () {
                        that.element.trigger("playrequest", [url, end]);
                    }).appendTo(timecode);
                }
                playlink.appendTo(timecode);
                timecode.prependTo(this);
            }
            // URL
            if ((url !== undefined) && (url !== o.resource_url)) {
                var urlarea = $('<div></div>').addClass("aa-playlist-title-url").prependTo(this);
                if (editurl) {
                    $("<a></a>").attr("href", editurl).text(url).appendTo(urlarea);
                } else {
                    urlarea.text(url);
                }
                if (start === undefined) {
                    playlink.appendTo(urlarea);
                }
            }
            /*
            $("<a href='#'>edit</a>").addClass("aa-button").click(function () {
                that.edit(thesection);
                return false;
            }).appendTo(sectionedit);
            */
        });

        this.element.mouseover(function () {
            $(this).addClass("mouseover");
        }).mouseleave(function () {
            $(this).removeClass("mouseover");
        }).resizable({
            stop: function () {
                if (o.annotation_id) {
                    $.ajax({
                        type: "POST",
                        url: o.size_url,
                        data: {
                            width: that.element.width(),
                            height: that.element.height()
                        }
                        // success: function () { $.log("size saved"); }
                    });
                }
            }
        }).draggable({
            handle: ".aa-playlist-play-menu, .aa-playlist-edit-menu",
            stop: function () {
                if (o.annotation_id) {
                    var pos = that.element.position();
                    $.ajax({
                        type: "POST",
                        url: o.position_url,
                        data: {
                            top: pos.top,
                            left: pos.left
                        }
                        // success: function () { $.log("position saved"); }
                    });
                }
            }
        });

        o._timelinesByID = {};        
        /*
        that.element.timedtext({
        });
        */
        $("[data-start]", this.element).each(function () {
            $(this).addClass("aa-playlist-section");
            var elt = $(this).get(0);
            var url = $(this).attr("data-url") || "";
            var start = $.timecode_tosecs_attr($(this).attr("data-start"));
            var end = $.timecode_tosecs_attr($(this).attr("data-end"));
            var timeline = that._timelineForID(url);
            // log("found section", url, start, end);
            timeline.add(elt, start, end);
        });
        
        o._display.autoscrollable();

        // enter the appropriate mode
        o.edit ? this.edit() : this.read();
    },

    _timelineForID: function (id) {
        var o = this.options;
        var timeline = o._timelinesByID[id];
        if (timeline !== undefined) return timeline;
        timeline = $.aa_timeline({
            show: function (elt) {
                $(elt).addClass("aa-playlist-section-active");
                if (o.mode == "single") {
                    $(elt).show();
                } else {
                    // log("scrollto", o._display, params.elt);
                    // if (!o._playing) {
                        o._display.autoscrollable("scrollto", elt);
                    //}
                }
            },
            hide: function (elt) {
                $(elt).removeClass("aa-playlist-section-active");
                if (o.mode == "single") {
                    $(elt).hide();
                }
            }
        });
        // log("created timeline for", id);
        o._timelinesByID[id]= timeline;
        return timeline;
    },

    doTimeUpdate : function (time, playerid) {
        // log("playlist.doTimeUpdate", time, playerid);
        var o = this.options;
        // if (o._playing) return;
        if (playerid === undefined) playerid = "";
        if (o._timelinesByID) {
            var timeline = this.options._timelinesByID[playerid];
            if (timeline) { timeline.setCurrentTime(time); }
        }
        // else { log("playlist: no timeline for", playerid); }
    },

    set_single_mode: function () {
        var o = this.options;
        if (o.mode === "single") return;
        $(".aa-playlist-section", this.element).not(".aa-playlist-section-active").hide();
        o.mode = "single";
    },
    
    set_list_mode: function () {
        var o = this.options;
        if (o.mode === "list") return;
        $(".aa-playlist-section", this.element).show();
        o.mode = "list";
    },

    _sectionSource: function (s, next_s, lastsection) {
        // TODO: suppress redundant endtimes
        var ret = "";
        var alwaysShowEndTimes = true;
        //$.log("sectionSource", lastsection);
        var next_start = next_s ? next_s.attr("data-start") : null;

        /* URL */
        var surl = s.attr("data-url");
        if (surl !== undefined) {
            // ideally this would also incorporate whether the preceding title had it's URL as well?
            if (surl != this.options.resource_url) {
                ret += surl + "\n";
            }
        }
        
        if (s.attr("data-start") !== undefined) {
            ret += $.timecode_fromsecs(s.attr("data-start")) + " -->";
            var end = s.attr("data-end");
            
            if (end && (alwaysShowEndTimes || (!lastsection && (!next_start || end !== next_start)))) {
                ret += " " + $.timecode_fromsecs(end) + "\n";
            } else {
                ret += "\n";
            }
        }
        // need to unescape this code
        // maybe for future / ie bugs?? unescaping entities: http://paulschreiber.com/blog/2008/09/20/javascript-how-to-unescape-html-entities/
        ret += $(".aa-playlist-section-source", s).val(); // html();
        ret += "\n\n";
        return ret;
    },

    edit: function (section) {
        // $('div#textile-help').show(); //show textile help
        var o = this.options;
        if (o._editing === true) return;
        if (o._playing === true) this.stop(); // needs to actually stop the playout...

        o._editmenu.show();
        o._playmenuhider.do_now();
        var display = $(".aa-playlist-display", this.element);
        // get the "percentage scroll position" of the display (from 0 = top, to 1 = bottom)
        var pscroll = display.scrollTop() / (display[0].scrollHeight - display.height());

        var sections = $(".aa-playlist-section", this.element).get();
        if (section) {
            o._editSection = section;
            var nextsection = undefined;
            var lastsection = false;
            for (var si=0; si<sections.length; si++) {
                // $.log("comparing", sections[si], section);
                if (sections[si] === section) {
                    // $.log("found section", si, sections.length);
                    nextsection = ((si+1) < sections.length) ? $(sections[si+1]) : undefined;
                    lastsection = (si == sections.length-1);
                    break;
                }
            }
            // $.log("edit single section, next=", nextsection); 
            var text = this._sectionSource($(section), nextsection, lastsection);
            o._textarea.val(text);
        } else {
            // FIX!
            o._editSection = null;
            var text = "";
            // $(".aa-playlist-section", that.elt).each(function () {
            for (var si=0; si<sections.length; si++) {
                var nextsection = ((si+1) < sections.length) ? $(sections[si+1]) : undefined;
                text += this._sectionSource($(sections[si]), nextsection, si+1>=sections.length);
            }
            o._textarea.val(text);
        }
        // $.log("undo", undoSource);

        window.setTimeout(function () {
            // set the textarea scroll to match the pscroll of the display
            var textarea = $(".aa-playlist-edit textarea", this.element);
            var st = pscroll * (textarea[0].scrollHeight - textarea.height());
            textarea.scrollTop(Math.floor(st));
        }, 250);

        o._editing = true;
        o._display.hide();
        o._edit.show();
        window.setTimeout(function () { o._textarea.focus() }, 100);
    },

    read: function () {
        var o = this.options;
        if (o._editing === false) return;
        o._editmenu.hide();
        o._editing = false;
        
        o._display.show();
        o._edit.hide();
    },

    play: function () {
        var o = this.options;
        if (o._playing === true) return;
        o._playbutton.addClass("aa-playlist-play-button-active");
        o._playing = true;
        // $("a.playlink:visible:first", this.element).trigger("click");
        $("a.playlink:first", this.element).trigger("click");
    },

    stop: function () {
        var o = this.options;
        if (o._playing === false) return;
        o._playbutton.removeClass("aa-playlist-play-button-active");
        o._playing = false;
    },

    save: function () {
        // $.log("SAVING");
        // $('div#textile-help').hide(); //hide textile help
        var o = this.options;
        if (o._saving) return;
        o._saving = true;
        var pos = this.element.position();
        var text = o._textarea.val();
        var data = {
            resource: o.resource_url,
            source: text,
            left: pos.left,
            top: pos.top,
            width: this.element.width(),
            height: this.element.height()
        };
        // $.log("text", text);
        if (o._editSection) {
            data.section_ids = $(o._editSection).attr("data-id");
        } else {
            var ids = [];
            $(".aa-playlist-section", this.element).each(function () { ids.push($(this).attr("data-id")) });
            data.section_ids = ids.join(",");
        }
        if (o.annotation_id) {
            data.annotation_id = o.annotation_id;
        }
        
        // saving feedback!
        this.element.addClass("aa-playlist-saving");
        
        var that = this;
        $.ajax({
            type: "POST",
            url: o.save_url,
            data: data,
            dataType: 'json',
            success: function (data) {
                // reset / reload
                that.element.remove(); // DROP THIS ELEMENT!
                o.load_annotation(data.annotation_id);
                // FORCE THE TAGSTOOL TO RELOAD!
                // $("#tagstool a.reload_button").trigger("click");
            },
            error: function (a, b, c) {
                log("error", a, b, c);
            }

        });
    },
    
	destroy: function() {
	    // log("destroy");

        // window.clearInterval(that.interval_id);
        // that.interval_id = null;
        // log("unbinding handler");
        var o = this.options;
        $("body").unbind("pastetimecode", o._pastetimecodehandler);
        // $("body").unbind("ended", o._endedhandler);
        
		this.element.removeClass( "aa-playlist" );
		$.Widget.prototype.destroy.apply( this, arguments );


	}

});

})(jQuery);

