/**
 * @fileoverview This file provides a few methods to convert timecodes to
 * seconds, and seconds to timecodes.
 * @author Michael Murtaugh <mm@automatist.org> and the Active Archive contributors
 * @license GNU AGPL
 */

/*
 * Change Log
 * ==========
 * 
 * 2 May 2010: timecode_tosecs returns null (not undefined) on no match
 */

(function ($) {

// hours optional
timecode_tosecs_pat = /^(?:(\d\d):)?(\d\d):(\d\d)(,(\d{1,3}))?$/;
// slightly less strict (allow erroneously long decimal part
// timecode_tosecs_pat = /^(?:(\d\d):)?(\d\d):(\d\d)(,(\d+))?$/;

function zeropad (n, toplaces) {
    var ret = ""+n;
    var foo = toplaces - ret.length;
    for (var i=0; i<foo; i++) {
        ret = "0"+ret;
    }
    return ret;
}

function zeropostpad (n, toplaces) {
    var ret = ""+n;
    var foo = toplaces - ret.length;
    for (var i=0; i<foo; i++) {
        ret = ret+"0";
    }
    return ret;
}

/**
 * Converts a timecode to seconds.  Seeks and returns first timecode pattern
 * and returns it in secs nb:.  Timecode can appear anywhere in string, will
 * only convert first match.  Note that the parseInt(f, 10), the 10 is
 * necessary to avoid "09" parsing as octal (incorrectly returns 0 then since 9
 * is an invalid octal digit). 
 * @private
 * @param {String} tcstr A string containing a timecode pattern.
 * @returns A timecode in secs nb.
 */
function timecode_tosecs (tcstr) {
    r = tcstr.match(timecode_tosecs_pat);
    if (r) {
        ret = 0;
        if (r[1]) {
            ret += 3600 * parseInt(r[1], 10);
        }
        ret += 60 * parseInt(r[2], 10);
        ret += parseInt(r[3], 10);
        if (r[5]) {
            ret = parseFloat(ret+"."+r[5]);
        }
        return ret;
    } else {
        return null;
    }
}

/**
 * Converts seconds to a timecode.  If fract is True, uses .xxx if either
 * necessary (non-zero) OR alwaysfract is True.
 * @private
 * @param {String} rawsecs A String containing a timecode pattern 
 * @param {String} fract A String
 * @param {Boolean} alwaysfract A Boolean
 * @returns A string in HH:MM:SS[.xxx] notation
 */
function timecode_fromsecs (rawsecs, fract, alwaysfract) {
    // console.log("timecode_fromsecs", rawsecs, fract, alwaysfract);
    if (fract === undefined) { fract = true; }
    if (alwaysfract === undefined) { alwaysfract = false; }
    // var hours = Math.floor(rawsecs / 3600);
    // rawsecs -= hours*3600;
    var hours = Math.floor(rawsecs / 3600);
    rawsecs -= hours * 3600;   
    var mins = Math.floor(rawsecs / 60);
    rawsecs -= mins*60;
    var secs;
    if (fract) {
        secs = Math.floor(rawsecs);
        rawsecs -= secs;
        if ((rawsecs > 0) || alwaysfract) {
            fract = zeropostpad((""+rawsecs).substr(2, 3), 3);
            // return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2)+","+fract;
            if (hours) {
                return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2)+","+fract;
            } else {
                return zeropad(mins, 2)+":"+zeropad(secs, 2)+","+fract;
            }
        } else {
            if (hours) {
                // return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2);
                return zeropad(hours, 2)+":"+ zeropad(mins, 2)+":"+zeropad(secs, 2);
            } else {
                // return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2);
                return zeropad(mins, 2)+":"+zeropad(secs, 2);
            }
        }
    } else {
        secs = Math.floor(rawsecs);
        // return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2);
        if (hours) {
            return zeropad(hours, 2)+":"+zeropad(mins, 2)+":"+zeropad(secs, 2);
        } else {
            return zeropad(mins, 2)+":"+zeropad(secs, 2);
        }
    }
}

/**
 * A lazy version of timecode_tosecs() that accepts both timecode strings and
 * seconds float/integer as parameter.
 * @private
 * @param {String|Integer|Float} val A timecode or seconds. 
 * @returns A timecode in secs nb
 */
function timecode_tosecs_attr(val) {
    if (val) {
        if (typeof(val) == "string") {
            var tc = timecode_tosecs(val);
            if (tc !== null) { return tc; }
        }
        return parseFloat(val);
    }
    return val;
}                        

// export
/**
 * Converts a timecode to seconds.  Seeks and returns first timecode pattern
 * and returns it in secs nb:.  Timecode can appear anywhere in string, will
 * only convert first match.  Note that the parseInt(f, 10), the 10 is
 * necessary to avoid "09" parsing as octal (incorrectly returns 0 then since 9
 * is an invalid octal digit). 
 * @function
 * @param {String} tcstr A string containing a timecode pattern.
 * @returns A timecode in secs nb.
 */
$.timecode_fromsecs = timecode_fromsecs;

/**
 * Converts seconds to a timecode.  If fract is True, uses .xxx if either
 * necessary (non-zero) OR alwaysfract is True.
 * @function
 * @param {String} rawsecs A String containing a timecode pattern 
 * @param {String} fract A String
 * @param {Boolean} alwaysfract A Boolean
 * @returns A string in HH:MM:SS[.xxx] notation
 */
$.timecode_tosecs = timecode_tosecs;

/**
 * A lazy version of timecode_tosecs() that accepts both timecode strings and
 * seconds float/integer as parameter.
 * @function
 * @param {String|Integer|Float} val A timecode or seconds. 
 * @returns A timecode in secs nb
 */
$.timecode_tosecs_attr = timecode_tosecs_attr;

// alternate names

/**
 * Shortcut for {@link $.timecode_tosecs_attr}
 * @function
 */
$.timecode_parse = timecode_tosecs_attr;

/**
 * Shortcut for {@link $.timecode_fromsecs}
 * @function
 */
$.timecode_unparse = timecode_fromsecs;

})(jQuery);

