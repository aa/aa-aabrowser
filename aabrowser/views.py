# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.

import SRT
import urlparse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from django.http import (
        HttpResponse,
        HttpResponseRedirect,
        HttpResponseBadRequest,
        Http404,
        HttpResponseForbidden,
        HttpResponseNotAllowed,
)
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from LinkForm import Link_parseText, name_normalize, name_urlunescape
from models import *
import urllib
import re
import django.forms
from .utils import audacity2srt

try:
    import simplejson as json
except ImportError:
    import json


def garbage_collect_tags():
    Tag.objects.filter(children__isnull=True).filter(sections__isnull=True)\
            .delete()


def home(request):
    """
    Redirects to the resource list ordered by the most recent changes.
    """
    return HttpResponseRedirect(reverse('aa-recent'))

colonname = re.compile(r"^\s*(\w+)\s*:\s*(.*)$", re.I)
coloncleaner = re.compile(r"^\s*\w+\s*:\s*", re.I)


def add(request):
    context = {}
    if request.method == "POST":
        s = request.POST.get("s", "").strip()
        if (not s.startswith("http")):
            m = colonname.match(s)
            function = ""
            if m:
                function, s = m.groups()

            url = reverse("aabrowser.views.search")

            if function.lower() == "tags" or function.lower() == "tag":
                s = name_normalize(s)
                url = reverse("aabrowser.views.playlist", args=[s])
            else:
                url = reverse("aabrowser.views.search")
                url += "?" + urllib.urlencode({'s': s})
            return HttpResponseRedirect(url)
        elif s:
            context['navurl'] = s
            try:
                r, status = getResource(s)
                context['resource'] = r
                if r:
                    r.status = "active"
                    r.save()
                    return HttpResponseRedirect(\
                            reverse("aabrowser.views.resource", args=[r.id]))

                context['status'] = status
                context['results'] = True
            except ValueError:
                context['message'] = "Bad URL, check your URL and try again \
                        (it should start with \"http://\")."
        else:
            return HttpResponseRedirect(reverse("aabrowser.views.resources"))
    context['resources'] = Resource.objects.filter(status="active")
    return render_to_response("add.html", context, \
            context_instance=RequestContext(request))


def resources(request, ordering="-db_lastmodified", rtype=None):
    """
    Returns a list of resources.
    It takes two optional parameters:
    - ordering (string) : an custom ordering
    - rtype (string) : a media type (audio/video/image) to filter by
    """
    garbage_collect_tags()
    context = {}

    if rtype is None:
        qs = Resource.objects.filter(status="active").order_by(str(ordering))
    elif rtype == "video":
        qs = Resource.objects.filter(status="active", \
                rtype__in=["audio/video", "video"]).order_by(str(ordering), \
                "-db_lastmodified")
    else:
        qs = Resource.objects.filter(status="active", rtype=rtype)\
                .order_by(str(ordering), "-db_lastmodified")

    paginator = Paginator(qs, 25)  # Show 25 contacts per page

    # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        context['resources'] = paginator.page(page)
    except (EmptyPage, InvalidPage):
        context['resources'] = paginator.page(paginator.num_pages)

    # Get the tags
    context['tags'] = Tag.objects.all().order_by('parent', 'name')

    return render_to_response("resources.html", context, \
            context_instance=RequestContext(request))


def resource(request, id):
    garbage_collect_tags()
    context = {}
    doreload = request.REQUEST.get("reload")
    download = request.REQUEST.get("download")
    r = get_object_or_404(Resource, pk=id)
    if download:
        r, status = getResource(r.url, download=True)
        context['status'] = status
        return HttpResponseRedirect(reverse("aabrowser.views.resource", \
                args=[id]))
    elif doreload:
        r, status = getResource(r.url, reload=True)
        context['status'] = status
        return HttpResponseRedirect(reverse("aabrowser.views.resource", \
                args=[id]))

    context['resource'] = r
    context['annotations'] = r.annotations.filter(source="")
    context['navurl'] = r.url
    context['tags'] = Tag.objects.order_by('parent', 'name')
    return render_to_response("resource.html", context, \
            context_instance=RequestContext(request))


def playlist(request, names=""):
    garbage_collect_tags()
    context = {}
    tag = Tag.objects.get(name=name_urlunescape(names.strip()))

    # get an "internal" resource object for this playlist
    url = tag.get_internal_url()
    (r, created) = Resource.objects.get_or_create(url=url)
    context['resource'] = r
    context['playlist'] = True
    context['annotations'] = r.annotations.filter(source="")
    context['playlist_sections'] = tag.sections\
            .exclude(annotation__resource=r).order_by("resource__url", "start")
    context['navurl'] = "Tags: " + tag.name  # (", ".join([x.name for x in context['framingtags']]))
    context['tags'] = Tag.objects.order_by('parent', 'name')
    return render_to_response("playlist.html", context, \
            context_instance=RequestContext(request))


def search(request):
    s = request.REQUEST.get("s", "").strip()
    s = coloncleaner.sub("", s)
    context = {}
    context['navurl'] = "Search: %s" % s
    context['search_text'] = s
    context['playlist_sections'] = Section.objects.filter(source__contains=s)
    # get an "internal" resource object for the search page (in general)
    (r, created) = Resource.objects.get_or_create(url="/search")
    context['resource'] = r
    context['playlist'] = True
    context['annotations'] = r.annotations.filter(source="")
    context['tags'] = Tag.objects.order_by('parent', 'name')
    return render_to_response("search.html", context, \
            context_instance=RequestContext(request))


def srt(request):
    context = {}
    if request.method == "POST":
        source = request.POST['source']
        p = SRT.SRTParser()
        context['srt'] = p.parseString(source)

    return render_to_response("srt.html", context, \
            context_instance=RequestContext(request))


def resource_annotate(request, id):
    url = request.REQUEST.get('resource')
    resource = get_object_or_404(Resource, pk=id, url=url)
    context = {}
    if request.method == "POST":
        annotation_id = request.POST.get('annotation_id')
        if annotation_id:
            annotation = get_object_or_404(Annotation, pk=annotation_id, \
                    resource=resource)
        else:
            annotation = Annotation.objects.create(resource=resource)

        # UPDATE POSITION/SIZE
        update = False
        left = request.REQUEST.get("left")
        if left != None:
            annotation.left = int(float(left))
            update = True
        top = request.REQUEST.get("top")
        if top != None:
            annotation.top = int(float(top))
            update = True
        width = request.REQUEST.get("width")
        if width != None:
            annotation.width = int(float(width))
            update = True
        height = request.REQUEST.get("height")
        if height != None:
            annotation.height = int(float(height))
            update = True
        if update:
            annotation.save()

        section_ids = request.POST.get("section_ids", "").split(",")
        section_ids = [x for x in section_ids if x]
        source = request.POST['source'].strip()
        annotation.update_from_text(source, section_ids)

        # force resource to update it's db_lastmodified field
        resource.save()

        return HttpResponse('{"ok": 1, "annotation_id": %d}' % annotation.id)

    return HttpResponseNotAllowed("post only")


def resource_position(request, id):
    resource = get_object_or_404(Resource, pk=id)
    context = {}
    if request.method == "POST":
        top = request.POST.get("top")
        left = request.POST.get("left")
        if (top != None and left != None):
            resource.top = int(float(top))
            resource.left = int(float(left))
            resource.save()
            return HttpResponse("{ok: 1}")
    else:
        return HttpResponseNotAllowed("post only")


def resource_size(request, id):
    resource = get_object_or_404(Resource, pk=id)
    context = {}
    if request.method == "POST":
        width = request.POST.get("width")
        height = request.POST.get("height")
        if (width != None and height != None):
            resource.width = int(width)
            resource.height = int(height)
            resource.save()
            return HttpResponse("{ok: 1}")
    else:
        return HttpResponseNotAllowed("post only")


def resource_delete(request, id):
    resource = get_object_or_404(Resource, pk=id)
    context = {}
    context['resource'] = resource
    if request.method == "POST":
        submit = request.POST.get("_submit")
        if submit == "delete":
            resource.delete_cached_files()
            resource.delete()
            return HttpResponseRedirect(reverse("aabrowser.views.resources"))
        else:
            return HttpResponseRedirect(reverse("aabrowser.views.resource", \
                    args=[resource.id]))
    return render_to_response("resource_delete.html", context, \
            context_instance=RequestContext(request))


def resource_info(request):
    """ return playinfo for a resource as json """
    id = request.REQUEST.get("id")
    if id:
        resource = get_object_or_404(Resource, pk=id)
    else:
        url = request.REQUEST.get("url")
        resource = get_object_or_404(Resource, url=url)

    context = {}
    data = {}
    data['id'] = resource.id
    data['url'] = resource.url
    data['cacheurl'] = resource.cache_url()
    data['rtype'] = resource.rtype
    delegate = resource.content_delegate()
    if delegate:
        data['delegate'] = delegate
    d = resource.duration()
    if d:
        data['duration'] = d
    if resource.content:
        if hasattr(resource.content, "width"):
            data['width'] = resource.content.width
        if hasattr(resource.content, "height"):
            data['height'] = resource.content.height
    return HttpResponse(json.dumps(data), mimetype="application/json")


def annotation(request, id):
    context = {}
    context['annotation'] = get_object_or_404(Annotation, pk=id)
    return render_to_response("annotation.html", context, \
            context_instance=RequestContext(request))


def annotation_position(request, id):
    annotation = get_object_or_404(Annotation, pk=id)
    if request.method == "POST":
        top = request.POST.get("top")
        left = request.POST.get("left")
        if (top != None and left != None):
            annotation.top = int(top)
            annotation.left = int(left)
            annotation.save()
            return HttpResponse('{"ok": 1, "annotation_id": %d}' \
                    % annotation.id)
    else:
        return HttpResponseNotAllowed("post only")


def annotation_size(request, id):
    annotation = get_object_or_404(Annotation, pk=id)
    if request.method == "POST":
        width = request.POST.get("width")
        height = request.POST.get("height")
        if (width != None and height != None):
            annotation.width = int(width)
            annotation.height = int(height)
            annotation.save()
            return HttpResponse('{"ok": 1, "annotation_id": %d}' \
                    % annotation.id)
    else:
        return HttpResponseNotAllowed("post only")


def annotation_delete(request, id):
    resource_url = request.REQUEST.get('resource')
    resource = get_object_or_404(Resource, url=resource_url)
    annotation = get_object_or_404(Annotation, pk=id, resource=resource)
    if request.method == "POST":
        annotation.delete()
        return HttpResponse('{"ok": 1}')
    else:
        return HttpResponseNotAllowed("post only")


def annotation_export(request, id, template="annotation.srt"):
    context = {}
    annotation = get_object_or_404(Annotation, pk=id)
    context['annotation'] = annotation
    # wrap sections to always have an explicit end time
    wsections = []
    sections = annotation.ssections()
    for (i, section) in enumerate(sections):
        aobj = {'obj': section, 'start': section.start, 'end': None}
        if aobj['start'] == None:
            aobj['start'] = 0.0
        if section.end != None:
            aobj['end'] = section.end
        else:
            if i + 1 < len(sections):
                aobj['end'] = sections[i + 1].start
            if aobj['end'] == None:
                aobj['end'] = aobj['start'] + 10
        wsections.append(aobj)
    context['sections'] = wsections
    return render_to_response(template, context, \
            context_instance=RequestContext(request), \
            mimetype="text/plain;charset=utf-8")


class ImportForm(django.forms.Form):
    file = django.forms.Field(widget=django.forms.FileInput, required=False)


def annotation_import_audacity(request, id):
    """
    Saves the file directly from the request object.
    Disclaimer:  This is code is just an example, and should
    not be used on a real website.  It does not validate
    file uploaded:  it could be used to execute an
    arbitrary script on the server.
    """
    a = Annotation.objects.get(pk=id)
    context = {}
    context['a'] = a
    if request.method == 'POST':
        form = ImportForm(request.POST, request.FILES)
        if form.is_valid():
            f = request.FILES['file']
            data = ""
            for chunk in f.chunks():
                data += chunk
            # removes current sections
            for section in a.sections.all():
                section.delete()
            # updates with the new content
            a.update_from_text(audacity2srt(data))
            return HttpResponse("Import complete, close this window and reload the page")
    else:
        # display the form
        form = ImportForm()
        context['form'] = form
        return render_to_response("annotation_import.html", context, \
                context_instance=RequestContext(request))


class TagForm(ModelForm):
    class Meta:
        model = Tag
        fields = ('parent', 'text')
        # exclude = ('name')


def tag(request, name):
    name = name_urlunescape(name.strip())
    tag = get_object_or_404(Tag, name=name)
    if request.method == 'POST':
        form = TagForm(request.POST, instance=tag)
        if form.is_valid():
            form.save()
    else:
        form = TagForm(instance=tag)

    context = {}
    context['tag'] = tag
    context['form'] = form
    return render_to_response("tag-edit.html", context, \
            context_instance=RequestContext(request))

from django import forms


class TagRenameForm(forms.Form):
    newname = forms.CharField(max_length=255, label="rename to", required=True)


def tag_rename(request, name):
    name = name_urlunescape(name.strip())
    tag = get_object_or_404(Tag, name=name)
    if request.method == 'POST':
        form = TagRenameForm(request.POST)
        if form.is_valid():
            pass
            repl = re.compile(r"\[\[\s*" + re.escape(tag.name) + r"\s*\]\]")
            for s in tag.sections.all():
                repl.sub(s.source, "[[" + form.cleaned_data.get("newname") \
                        + "]]")
    else:
        form = TagRenameForm()

    context = {}
    context['tag'] = tag
    context['form'] = form
    return render_to_response("tag-rename.html", context, \
            context_instance=RequestContext(request))


def tagstool(request):
    context = {}
    garbage_collect_tags()
    context['tags'] = Tag.objects.all().order_by('parent', 'name')
    return render_to_response("tagstool.html", context, \
            context_instance=RequestContext(request))


def linktool(request):
    context = {}
    garbage_collect_tags()
    # context['tags'] = Tag.objects.all().order_by('parent', 'name')
    return render_to_response("linktool.html", context, \
            context_instance=RequestContext(request))


def linktool_search(request):
    context = {}
    t = request.REQUEST.get("t", "")
    if t:
        context['tags'] = Tag.objects.filter(name__contains=t).order_by('name')
        context['resources'] = Resource.objects.filter(url__contains=t)\
                .order_by('url')
    else:
        context['tags'] = []
        context['resources'] = []

    return render_to_response("linktool_search.html", context, \
            context_instance=RequestContext(request))

import math


def columnize(things, n=3):
    numthings = len(things)
    columns = [[] for i in range(n)]
    i = 0
    for c in range(n):
        colheight = math.ceil(float(numthings) / (n - c))
        for y in range(colheight):
            if i >= len(things):
                break
            columns[c].append(things[i])
            numthings -= 1
            i += 1
    return columns


def tags(request):
    context = {}
    context['tags'] = list(Tag.objects.order_by('name'))
    context['tags_columns'] = columnize(context['tags'])
    return render_to_response("tags.html", context, \
            context_instance=RequestContext(request))


def searchandreplace(request):
    t = request.REQUEST.get("t", "")
    context = {}
    return render_to_response("searchandreplace.html", context, \
            context_instance=RequestContext(request))
