# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.

"""
ASRT.py
"Active" SRT parser...
a modified SRT parser

SEQNUMBER
URL[#options]
START --> STOP

as an exception, deal with leading numbers a la SRT?

either the URL, the TIMECODE, or both need to be present
"""

import cgi  # for cgi.escape of titles
import re
import sys
import timecode


splitpat = re.compile(
    r"""^
    (?# OPTION 1. URL + optionally a timecode )
    (
    ((?P<urlseq>\d+)\r?\n)?
    ((?P<url>http://.+)\r?\n)
    ((?P<urlstart> ((\d\d):)? (\d\d): (\d\d) ([,.]\d{1,3})?)
    \s* --> \s*
    (?P<urlend> ((\d\d):)? (\d\d): (\d\d) ([,.]\d{1,3})?)?)?
    \s*)

    |

    (?# OPTION 2. just a timecode )
    (^
    ((?P<seq>\d+)\r?\n)?
    (?P<start> ((\d\d):)? (\d\d): (\d\d) ([,.]\d{1,3})?)
    \s* --> \s*
    (?P<end> ((\d\d):)? (\d\d): (\d\d) ([,.]\d{1,3})?)?
    \s*)
    $""",
    re.X | re.M
)


def split(text):
    pos = 0
    bodies = []
    heads = [None]
    for m in splitpat.finditer(text):
        start = m.start()
        pre = text[pos:start]
        pos = m.end()
        bodies.append(pre)
        heads.append(m.groupdict())
    bodies.append(text[pos:])
    return zip(heads, bodies)


def parse(text):
    """
    returns [({headers}, "body")]
    with header keys: [optional]
    seq, url, [hash], start, [start_tc], end, [end_tc]
    """

    if hasattr(text, "read"):
        text = text.read()

    ret = []
    for (h, b) in split(text):
        b = b.strip()
        if (h == None and b == ""):
            continue

        if h == None:
            h = {'url': None, 'start': None, 'end': None}
        elif 'url' in h and h["url"] != None:
            url = h['url'].strip()
            h['seq'] = h['urlseq']
            h['start'] = h['urlstart']
            h['end'] = h['urlend']
            svals = url.split("#", 1)
            if len(svals) == 2:
                (h['url'], h['hash']) = svals
            else:
                (h['url'], h['hash']) = (url, None)

        if h:
            if h['start']:
                h['start_tc'] = h['start']
                h['start'] = timecode.timecode_tosecs(h['start'])
            if h['end']:
                h['end_tc'] = h['end']
                h['end'] = timecode.timecode_tosecs(h['end'])

        ret.append((h, b))

    return ret


if (__name__ == "__main__"):

    test = """
    http://automatist.org#foo
    """
    from pprint import pprint

#    for m in urlpat.finditer(test.strip()):
#        pprint (m.groupdict())

#    for m in splitpat.finditer(sys.stdin.read()):
#        print "match"
#        print m.groupdict()

#    pprint (split(sys.stdin.read()))
#    pprint (parse(sys.stdin.read()))
    for (h, b) in parse(sys.stdin.read()):
        print h
        print b
        print "=" * 20
