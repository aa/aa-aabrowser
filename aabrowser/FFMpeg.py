# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


# FFMpeg

import os
# import popen2
import re
import sys
from django.conf import settings
from subprocess import Popen, PIPE, STDOUT

DEBUG = 0


def streamInfoGetComponent(sinfo, cname):
    for rec in sinfo:
        if (rec[0] == cname):
            return rec[1]


def streamInfoGetVideoSize(sinfo):
    vidinfo = streamInfoGetComponent(sinfo, 'video')
    if (vidinfo != None):
        parts = vidinfo.split(",")
        for part in parts:
            part = part.strip()
            if part.find('x') >= 0:
                ret = part.split("x")[:2]
                return map(lambda x: int(x), ret)
    return (-1, -1)


def getStreamInfo(path):
    ## HACK TO FIX WEIRD LOCAL FILE SEGFAULT...
    ## turn a local URL into a local path
    if path.startswith(settings.MEDIA_URL):
        path = path.replace(settings.MEDIA_URL, settings.MEDIA_ROOT)

    ret = {}
    results = do('-i "%s"' % path)
    duration = 0

    for line in results.splitlines():
        line = line.strip().lower()

        results = re.search("duration:\s*(\d\d):(\d\d):(\d\d)\.(\d+)?", \
                line, re.IGNORECASE)
        if results:
            results = results.groups()
            duration = (int(results[0]) * 60 * 60) + (int(results[1]) * 60) \
                    + int(results[2])
            if len(results) > 3:
                duration = float(str(duration) + "." + results[3])
            else:
                duration = float(duration)

        results = re.search(r"stream #(\d+)\.(\d+).*(audio|video): (.*)", \
                line, re.IGNORECASE)
        if results:
            results = results.groups()
            stream_num = int(results[1])
            type = results[2]
            info = results[3]
            ret[type] = info

    # FURTHER EXTRACT DATA
    # DURATION
    if (duration > 0.0):
        ret['duration'] = duration

    # VIDEO
    if 'video' in ret:
        # SIZE (width, height)
        sizepat = r"(\d+)x(\d+),?\s*"
        r = re.search(sizepat, ret['video'])
        if r:
            ret['width'] = int(r.group(1))
            ret['height'] = int(r.group(2))
        # remove the size from the video info string
        ret['video'] = re.sub(sizepat, "", ret['video'])

        # FRAMERATE (fps)
        fpspat = r"(\d+(\.\d+)?)\s*fps(\(.*\))?\s*"
        r = re.search(fpspat, ret['video'])
        if r:
            ret['fps'] = float(r.group(1))
        # remove the size from the video info string
        ret['video'] = re.sub(fpspat, "", ret['video'])
        # cleanup
        ret['video'] = ret['video'].strip(" ,")

    return ret


def do(params):
    """ added limiter on repeated lines in response to an apparent ffmpeg bug
    that seems to loop outputting an error message """

    cmd = settings.FFMPEGPATH + ' ' + params

    if DEBUG:
        print "FFMPEG", params

#    subprocess = popen2.Popen4(cmd)
#    pout = subprocess.fromchild
#    pin = subprocess.tochild
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, \
            close_fds=True)
    (pin, pout) = (p.stdin, p.stdout)

    results = ''
    lastline = ''
    repeatcount = 0
    while 1:
        outline = pout.readline()
        if not outline:
            break
        if DEBUG:
            print "LINE:", outline
        if (outline == lastline):
            repeatcount += 1
            if repeatcount > 10:
                break
        else:
            results += outline
        lastline = outline

    pin.close()
    pout.close()
    os.kill(p.pid, 1)

    return results


def normalize(params):
    cmd = "%s %s" % (NORMALIZEPATH, params)
    if DEBUG:
        print cmd
    pout = os.popen(cmd)
    while 1:
        line = pout.readline()
        if not line:
            break
    pout.close()
    return


def getThumbnail(path, size="160x120", time=None):
    # size is parameter to "convert -resize "
    # so imagemagick options (like !) can be used

    ## HACK TO FIX WEIRD LOCAL FILE SEGFAULT...
    ## turn a local URL into a local path
    if path.startswith(settings.MEDIA_URL):
        path = path.replace(settings.MEDIA_URL, settings.MEDIA_ROOT)

    mi = None
    if time == None:
        time = 0.0
    elif type(time) == str:
        if time.endswith("%"):
            mi = getStreamInfo(path)
            time = mi.get('duration', 0) * (float(time[:-1]) / 100.0)
        else:
            time = float(time)

    if time > 0.0:
        # ensure <= duration
        if mi == None:
            mi = getStreamInfo(path)
        time = min(mi.get('duration', 0.0), time)

    # original shell pipeline:
    # ffmpeg -i input.mp4 -ss 15 -an -t 0:0:0.001 -f image2 - 2> /dev/null |\
    # convert jpg:- -resize 160x120 jpg:-

    # version for shell=False if for some reason that is necessary...
    # cmd1 = [settings.FFMPEGPATH, "-i", path, "-ss", time, "-an","-t","0:0:0.001","-f","image2","-"]
    # p1 = Popen(cmd1, stdout=PIPE, stderr=open('/dev/null', 'w'))
    # cmd1 = settings.FFMPEGPATH + " -i \"%s\" -ss %s -an -t 0:0:0.001 -f image2 - 2> /dev/null"
    cmd1 = settings.FFMPEGPATH + \
            " -i \"%s\" -ss %s -an -dframes 1 -f image2 - 2> /dev/null"
    cmd1 %= (path, str(time))
    p1 = Popen(cmd1, stdout=PIPE, shell=True)
    cmd2 = settings.CONVERTPATH + " jpg:- -resize %s jpg:- 2> /dev/null"
    cmd2 %= (size)
    p2 = Popen(cmd2, stdin=p1.stdout, stdout=PIPE, shell=True)
    return p2.communicate()[0]


if (__name__ == "__main__"):
    import sys
    try:
        src = sys.argv[1]
    except IndexError:
        src = "Videos/jodi.VOB"

    mi = getStreamInfo(src)
    print mi
