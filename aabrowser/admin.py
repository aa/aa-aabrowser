# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


from django.contrib import admin
from models import *
from models.Resource import getResource


####################
### Resource

def action_check(modeladmin, request, queryset):
    for elt in queryset:
        getResource(elt.url, check=True)

action_check.short_description = 'Check selected resources'


def action_reload(modeladmin, request, queryset):
    for elt in queryset:
        getResource(elt.url, reload=True)
        content = elt.content
        if elt.isDownloaded() and content:
            content.syncFile()

action_reload.short_description = 'Reload selected resources'


def action_download(modeladmin, request, queryset):
    for elt in queryset:
        # getResource(elt.url, download=True)
        elt.download()

action_download.short_description = 'Download selected resources'


class ResourceAdmin(admin.ModelAdmin):
    list_display = (
        'status',
        'isDownloaded',
        'url',
        'rtype',
        'content_type',
        'content_length',
        'last_modified',
        'etag',
        'content',
        'last_contact',
        'last_downloaded',
        'db_creator',
        'db_created',
        'db_lastmodifier',
        'db_lastmodified',
    )
    list_display_links = ('url',)
    list_filter = (
        'status',
        'rtype',
        'hostname',
    )
    search_fields = ('url',)
    date_hierarchy = 'last_modified'
    actions = [
        action_check,
        action_reload,
        action_download,
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'db_creator', None) is None:
            obj.db_creator = request.user
        obj.db_lastmodifier = request.user
        obj.save()

admin.site.register(Resource, ResourceAdmin)


##############
# SectionTag

class SectionTagAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'section',
        'rel',
        'tag',
    )
    raw_id_fields = ('section',)

admin.site.register(SectionTag, SectionTagAdmin)

##############
# TAG


class TagAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'parent',
        'child_count',
        'section_count',
    )
    list_editable = ('parent',)
    search_fields = ('name',)
    raw_id_fields = ('parent',)

admin.site.register(Tag, TagAdmin)

#############
# Image


class ImageAdmin(admin.ModelAdmin):
    list_display = (
        'resource',
        'format',
        'gray',
        'width',
        'height',
    )
    raw_id_fields = ('resource',)

admin.site.register(Image, ImageAdmin)

#############
# Media


class VideoAdmin (admin.ModelAdmin):
    list_display = (
        'resource',
        'bitrate_display',
        'duration',
        'video_format',
        'width',
        'height',
        'fps',
        'audio',
        'audio_format',
    )
    raw_id_fields = ('resource',)

admin.site.register(Video, VideoAdmin)


class AudioAdmin (admin.ModelAdmin):
    list_display = (
        'resource',
        'bitrate_display',
        'duration',
        'audio_format',
    )
    raw_id_fields = ('resource',)

admin.site.register(Audio, AudioAdmin)


class Mp3AudioAdmin (admin.ModelAdmin):
    list_display = (
        'resource',
        'bitrate_display',
        'duration',
        'audio_format',
        'title',
        'artist',
        'album',
        'year',
        'genre',
    )
    raw_id_fields = ('resource',)

admin.site.register(Mp3Audio, Mp3AudioAdmin)


class SectionInline(admin.TabularInline):
    model = Section


class AnnotationAdmin (admin.ModelAdmin):
    list_display = (
        'resource',
        'source',
        'db_creator',
        'db_created',
    )
    raw_id_fields = ('resource',)
    search_fields = (
        'resource__url',
        'source',
    )
    inlines = [
        SectionInline,
    ]

admin.site.register(Annotation, AnnotationAdmin)


class SectionAdmin (admin.ModelAdmin):
    list_display = (
        'annotation',
        'resource',
        'source',
        'start',
        'end',
        'db_creator',
        'db_created',
        'db_lastmodifier',
        'db_lastmodified',
    )
    raw_id_fields = ('annotation',)
    search_fields = (
        'source',
        'resource__url',
        'annotation__resource__url',
    )

admin.site.register(Section, SectionAdmin)


#############
# YouTube


class YouTubeVideoAdmin(admin.ModelAdmin):
    list_display = (
        'youtube_id',
        'resource',
        'title',
        'duration',
        'published',
        'location',
        'category',
        'tags',
    )
    list_display_links = (
        'youtube_id',
        'resource',
    )
    raw_id_fields = ('resource', )
    search_fields = (
        'title',
        'description',
        'category',
        'tags',
    )
    date_hierarchy = 'published'

admin.site.register(YouTubeVideo, YouTubeVideoAdmin)


#############
# Viimeo


class VimeoVideoAdmin(admin.ModelAdmin):
    list_display = (
        'resource',
        'title',
        'duration',
        'upload_date',
        'description',
        'tags',
    )
    raw_id_fields = ("resource",)

admin.site.register(VimeoVideo, VimeoVideoAdmin)
