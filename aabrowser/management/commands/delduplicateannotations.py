from django.core.management.base import BaseCommand, CommandError
from aabrowser.models import Resource


class Command(BaseCommand):
    args = '[resid ...]'
    help = 'delete duplicate sections in each of the given resources annotations'

    def handle(self, *args, **options):
        for resid in args:
            res = Resource.objects.get(pk=resid)
            for a in res.annotations.filter(source=""):
                sections = {}
                for s in a.sections.all():
                    key = (s.start, s.end, s.source)
                    if key in sections:
                        print "deleting duplicate %d" % s.id
                        s.delete()
                    else:
                        sections[key] = True
