from django.core.management.base import BaseCommand, CommandError
from aabrowser.models import Annotation
from aabrowser.views import garbage_collect_tags

MAP = {
    'Speaker': 'People',
    'Project': 'Project',
    'Status': 'Status'
}


class Command(BaseCommand):
    args = '<>'
    help = ''

    def handle(self, *args, **options):
        f = args[0]
        t = args[1]
        print "replacing '%s' with '%s' in all annotations" % (f, t)
        repl = 0
        for a in Annotation.objects.all():
            text = a.get_text()
            if f in text:
                print "========"
                print a
                print text
                print "====>"
                text = text.replace(f, t)
                print text
                a.sections.all().delete()
                a.update_from_text(text)
                repl += 1

        garbage_collect_tags()
