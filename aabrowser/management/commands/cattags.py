from django.core.management.base import BaseCommand, CommandError
from aabrowser.models import Resource, Annotation, Tag

MAP = {
    'Speaker': 'People',
    'Project': 'Project',
    'Status': 'Status'
}


class Command(BaseCommand):
    args = '<>'
    help = ''

    def handle(self, *args, **options):
        for t in Tag.objects.all():
            if t.parent == None:
                # print t.name
                rel = None
                for link in t.sectiontag_set.all():
                    if link.rel:
                        rel = link.rel
                        break
                if rel:
                    # print "\t", rel
                    ptagname = MAP.get(rel)
                    (ptag, created) = Tag.objects.get_or_create(name=ptagname)
                    print "%s -> %s" % (t.name, ptagname)
                    t.parent = ptag
                    t.save()
